package com.cpe.Card.card.controller;

import com.cpe.Card.card.model.CardModel;
import com.cpe.Card.card.model.GenerationConfig;
import com.cpe.Card.card.model.Rarity;
import com.cpe.Card.card.model.minmax;
import com.cpe.Card.card.utils.http;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class CardService {

	private static final String USER_SERVER_URL = "http://34.231.202.160/";
	private static final String NODE_NOTIFICATION_SERVER = "http://34.196.125.97:8081/";
	private static final String NODE_S3_SERVER_URL = "http://34.196.125.97/";
	private static final String PYTHON_SERVER_URL = "http://18.210.123.32/postPhoto";
	private final CardRepository cardRepository;

	public CardService(CardRepository cardRepository) {
		this.cardRepository = cardRepository;
	}

	public List<CardModel> getAllCards() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public CardModel getCard(String id) {
		return cardRepository.findById(Integer.valueOf(id)).orElse(null);
	}


	public Optional<CardModel> getCard(Long id) {
		return cardRepository.findById(id);
	}

	public void addCard(CardModel card) {
		cardRepository.save(card);
	}

	public void generateCardProperties(CardModel card) {
		card.setDateCapture(new Date());
		card.setLevel(1);
		//Forces the rarity to random
		card.setRarity(getRandomRarity());
		//Forces random base power depending on rarity
		Integer basePower = getRandomBasePower(card.getRarity());
		card.setBasePower(basePower);
		card.setPower(basePower);
		cardRepository.save(card);
	}

	public void updateCard(CardModel card) {
		cardRepository.save(card);
	}

	public void deleteCard(Long id) {
		cardRepository.deleteById(id);
	}



	public Rarity getRandomRarity() {
		GenerationConfig config = new GenerationConfig();
		Map<Rarity,Double> map = config.getChancesOfDraw();
		double r = Math.random() * 100;
		double chances = 0;
		Rarity rarity = null;
		Iterator itrmap = map.entrySet().iterator();
		while (itrmap.hasNext() && rarity == null) {
			Map.Entry entry = (Map.Entry) itrmap.next();
			chances = chances + (Double) entry.getValue();
			if ( r < chances) {
				rarity = (Rarity) entry.getKey();
			}
		}
		return rarity;
	}

	public Integer getRandomBasePower(Rarity rarity) {
		GenerationConfig config = new GenerationConfig();
		Map<minmax,Integer> map = config.getMinMaxBasePower(rarity);
		Integer basePower = (int) Math.round( Math.random() * (map.get(minmax.MAX) - map.get(minmax.MIN)) + map.get(minmax.MIN));
		return basePower;
	}

	public boolean isLevelOk(CardModel source, CardModel target) {
		return source.getLevel() == target.getLevel();
	}

	public boolean isRarityOk(CardModel source, CardModel target) {
		return (source.getRarity().ordinal() == target.getRarity().ordinal() - 1) || (source.getRarity().ordinal() == Rarity.COMMON.ordinal() && target.getRarity().ordinal() == Rarity.COMMON.ordinal());
	}

	public boolean isLvlUpPossible(CardModel source, CardModel target) {
		if (!(isLevelOk(source,target))){
			return false;
		}
		if (!(isRarityOk(source,target))) {
			return false;
		}
		return true;
	}

	public boolean lvlUp(Long userId, Long idsource, Long idtarget) throws ExecutionException, InterruptedException {
		CardModel source = cardRepository.findById(idsource).get();
		CardModel target = cardRepository.findById(idtarget).get();
		if (source == null || target == null) {
			System.err.println("Lvl up not possible, one of the cards does no exists");
			return false;
		}
		if (source.getUserId() != target.getUserId()) {
			System.err.println("Both cards do not belong to the same user.");
			return false;
		}
		if (isLvlUpPossible(source, target)) {
			Integer newPower = target.getPower() + 2 * source.getPower() +  target.getBasePower();
			Integer price = newPower - target.getPower();
			if(http.asynchronousRequest(USER_SERVER_URL + "users/payPrice/" + userId + "/" + price, "POST", null)){
				target.setPower(newPower);
				target.setLevel(target.getLevel() + 1);
				cardRepository.save(target);
				this.deleteCard(idsource);
				return true;
			}

		}
		return false;
	}

	public List<CardModel> getAllCardsByUserId(Long id) {
		return cardRepository.findAllByUserId(id);
	}

	public CardModel getCardByImgUrl(String imgUrl) {
		return cardRepository.findByImgUrl(imgUrl);
	}

	public List<CardModel> getCardsByLocation(double latitude, double longitude) {
		List<CardModel> lc = cardRepository.findCardsByLatitudeAndLongitude(latitude, longitude);
		return lc;
	}

	public boolean buyCardUser(Long id) throws ExecutionException, InterruptedException {
		return http.asynchronousRequest(USER_SERVER_URL + "users/" + id + "/purchase", "POST", null);
	}

	public boolean canUserBuy(Long id) throws ExecutionException, InterruptedException {
		return http.asynchronousRequest(USER_SERVER_URL + "users/" + id + "/canPurchase", "POST", null);
	}

	public boolean generateCard(CardModel card) throws ExecutionException, InterruptedException {
		if(canUserBuy(card.getUserId())){
			Long userId = card.getUserId();
			card.setUserId(null);
			this.generateCardProperties(card);
			JsonObject json = new JsonObject();
			json.addProperty("imgUrl", card.getImgUrl());
			json.addProperty("userId", userId);
			System.err.println("Send to IA");
			return http.asynchronousRequest(PYTHON_SERVER_URL, "POST", json);
		}
		return false;
	}

	public Boolean iaSuccess(CardModel cardInput) throws ExecutionException, InterruptedException {
		CardModel card = this.getCardByImgUrl(cardInput.getImgUrl());
		if (card != null) {
			card.setPredictedClass(cardInput.getPredictedClass());
			card.setUserId(cardInput.getUserId());
			boolean isPurchased = this.buyCardUser(card.getUserId());
			if (isPurchased) {
				this.updateCard(card);
				JsonObject json = card.toJSON();
				return http.asynchronousRequest(NODE_NOTIFICATION_SERVER + "cardMinted", "POST", json);
			}
		}
		JsonObject json = card.toJSON();
		json.addProperty("fail", true);
		return http.asynchronousRequest(NODE_NOTIFICATION_SERVER + "cardMinted", "POST", json);


	}

	public Boolean iaFailure(CardModel cardInput) throws ExecutionException, InterruptedException {
		JsonObject json = cardInput.toJSON();
		json.addProperty("fail", true);
		http.asynchronousRequest(NODE_S3_SERVER_URL + "removePhoto", "POST", json);
		return http.asynchronousRequest(NODE_NOTIFICATION_SERVER + "cardMinted", "POST", json);
	}

	public boolean findCard(CardModel card, Long userId) throws ExecutionException, InterruptedException {
		CardModel cardModel = this.getCard(card.getId()).orElse(null);
		if(cardModel.getUserId() != userId) {
			if (cardModel != null) {
				if (!cardModel.getIsFound()) {
					JsonObject json = new JsonObject();
					json.addProperty("userId", userId);
					json.addProperty("golds", cardModel.getBasePower());
					if (http.asynchronousRequest(USER_SERVER_URL + "users/addGolds", "POST", json)) {
						cardModel.setIsFound(true);
						cardRepository.save(cardModel);
						return true;
					}
				}
			}
		}
		return false;
	}
}
