package com.cpe.Card.card.controller;

import com.cpe.Card.card.model.CardModel;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

public interface CardRepository extends CrudRepository<CardModel, Integer> {

    List<CardModel> findAllByUserId(Long id);

    CardModel findByImgUrl(String imgUrl);

    String req = "SELECT * FROM card_model c where c.user_id is not null and c.latitude < :latitude + 0.001 and c.latitude > :latitude - 0.001 and c.longitude < :longitude + 0.001 and c.longitude > :longitude - 0.001";
    @Query(value= req, nativeQuery=true)
    List<CardModel> findCardsByLatitudeAndLongitude(@Param("latitude") double latitude, @Param("longitude") double longitude);

    Optional<CardModel> findById(Long id);

    @Transactional
    void deleteById(Long id);
}
