package com.cpe.Card.card.esb;

/*
import com.cpe.Card.card.controller.CardService;
import com.cpe.Card.card.model.CardModel;
import com.model.Action;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Consumer implements ErrorHandler {

    @Autowired
    private CardService cardService;

    @Autowired
    JmsTemplate jmsTemplate;

    public Consumer(CardService cardService) {
        this.cardService = cardService;
    }

    @JmsListener(destination = "handle.card.action", containerFactory = "connectionFactory")
    public void receiveMessage(Action action) {
        System.err.println("Action reçue: " + action.getAction());
        if (action.getAction().equals("addCard")) {
            cardService.addCard((CardModel) action.getObject());
        } else if (action.getAction().equals("updateCard")) {
            cardService.updateCard((CardModel) action.getObject());
        } else if (action.getAction().equals("deleteCard")) {
            cardService.deleteCard((action.getObject()).toString());
        }

    }
    @Override
    public void handleError(Throwable t) {
        log.error("Error in listener", t);
    }
}*/