package com.cpe.Card.card.controller;

import com.cpe.Card.card.model.CardModel;
import com.cpe.Card.card.utils.http;
import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

//ONLY FOR TEST NEED ALSO TO ALLOW CROSS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController {
	private static final Integer NO_USER_ID = -1;


	private final CardDispatcher cardDispatcher;

	public CardRestController(CardDispatcher cardDispatcher) {
		this.cardDispatcher = cardDispatcher;
	}

	@RequestMapping("/cards")
	private List<CardModel> getAllCards() {
		List<CardModel> cardList = new ArrayList<CardModel>();
		for (CardModel cM : cardDispatcher.getAllCards()) {
			cardList.add(cM);
		}
		return cardList;

	}

	@RequestMapping("/cards/getCard/{id}")
	private CardModel getUser(@PathVariable String id) {
		System.err.println("ia sucess");
		CardModel card;
		card = cardDispatcher.getCard(id);
		return card;
	}


	@RequestMapping(method = RequestMethod.POST, value = "/cards/iaFailure")
	public Boolean iaFailure(@RequestBody CardModel cardInput) throws ExecutionException, InterruptedException {
		CardModel card = cardDispatcher.getCardByImgUrl(cardInput.getImgUrl());
		cardDispatcher.deleteCard(card.getId());
		System.err.println("IA Fail");
		return cardDispatcher.iaFailure(cardInput);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards/iaSuccess")
	public Boolean iaSuccess(@RequestBody CardModel cardInput) throws ExecutionException, InterruptedException {
		System.err.println("IA Success");
		return cardDispatcher.iaSuccess(cardInput);
	}

	@RequestMapping("/cards/user/{userId}")
	public List<CardModel> getCardsOfUser(@PathVariable Long userId) {
		return cardDispatcher.getAllCardsByUserId(userId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards/generateCard")
	public boolean generateCard(@RequestBody CardModel card) throws ExecutionException, InterruptedException {
		return cardDispatcher.generateCard(card);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards")
	public void addCard(@RequestBody CardModel card) {
		cardDispatcher.addCard(card);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/cards/{id}")
	public void updateCard(@RequestBody CardModel card, @PathVariable String id) {
		card.setId(Long.valueOf(id));
		cardDispatcher.updateCard(card);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/cards/{id}")
	public void deleteCard(@PathVariable String id) {
		cardDispatcher.deleteCard(Long.valueOf(id));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cards/{latitude}/{longitude}")
	public List<CardModel> getCardsByLocation(@PathVariable String latitude,@PathVariable String longitude) {
		return cardDispatcher.getCardsByLocation(Double.valueOf(latitude), Double.valueOf(longitude));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards/lvlup/{userId}/{idsource}/{idtarget}")
	public boolean lvlUp(@PathVariable String userId, @PathVariable String idsource, @PathVariable String idtarget) throws ExecutionException, InterruptedException {
		return cardDispatcher.lvlUp(Long.valueOf(userId), Long.valueOf(idsource),Long.valueOf(idtarget));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cards/findCard/{userId}")
	public boolean findCard(@RequestBody CardModel card, @PathVariable Long userId) throws ExecutionException, InterruptedException {
		return cardDispatcher.findCard(card, userId);
	}

	@RequestMapping(value = "/cards/unfindAll")
	public void unfindAll()  {
		List<CardModel> cardModelList = cardDispatcher.getAllCards();
		for(CardModel card: cardModelList){
			card.setIsFound(false);
			cardDispatcher.updateCard(card);
		}
	}

}
