package com.cpe.Card.card.model;

import java.util.HashMap;
import java.util.Map;

public class GenerationConfig {
    Map<Rarity, Double> chancesOfDraw;

    Map<Rarity, Map<minmax,Integer>> basePower;

    public GenerationConfig() {
        //Configure the chances to draw every rarity of card
        chancesOfDraw = new HashMap<>();
        chancesOfDraw.put(Rarity.COMMON, 50.0);
        chancesOfDraw.put(Rarity.UNCOMMON, 35.0);
        chancesOfDraw.put(Rarity.RARE, 10.0);
        chancesOfDraw.put(Rarity.EPIC, 4.5);
        chancesOfDraw.put(Rarity.LEGENDARY, 0.45);
        chancesOfDraw.put(Rarity.GODLIKE, 0.05);

        //Configure the possible base power for every rarity
        basePower = new HashMap<>();

        //Common
        Map<minmax,Integer> basePowerCommon = new HashMap<>();
        basePowerCommon.put(minmax.MIN,1);
        basePowerCommon.put(minmax.MAX,5);
        basePower.put(Rarity.COMMON,basePowerCommon);

        //Uncommon
        Map<minmax,Integer> basePowerUncommon = new HashMap<>();
        basePowerUncommon.put(minmax.MIN,3);
        basePowerUncommon.put(minmax.MAX,10);
        basePower.put(Rarity.UNCOMMON,basePowerUncommon);

        //Rare
        Map<minmax,Integer> basePowerRare = new HashMap<>();
        basePowerRare.put(minmax.MIN,5);
        basePowerRare.put(minmax.MAX,15);
        basePower.put(Rarity.RARE,basePowerRare);

        //Epic
        Map<minmax,Integer> basePowerEpic = new HashMap<>();
        basePowerEpic.put(minmax.MIN,50);
        basePowerEpic.put(minmax.MAX,100);
        basePower.put(Rarity.EPIC,basePowerEpic);

        //Legendary
        Map<minmax,Integer> basePowerLegendary = new HashMap<>();
        basePowerLegendary.put(minmax.MIN,100);
        basePowerLegendary.put(minmax.MAX,200);
        basePower.put(Rarity.LEGENDARY,basePowerLegendary);

        //Godlike
        Map<minmax,Integer> basePowerGodlike = new HashMap<>();
        basePowerGodlike.put(minmax.MIN,300);
        basePowerGodlike.put(minmax.MAX,600);
        basePower.put(Rarity.GODLIKE,basePowerGodlike);
    }

    public Map<minmax,Integer> getMinMaxBasePower(Rarity rarity) {
        return this.basePower.get(rarity);
    }

    public Map<Rarity,Double> getChancesOfDraw() {
        return this.chancesOfDraw;
    }
}
