package com.cpe.Card.card.utils;


import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;

public class http {

    public static Boolean asynchronousRequest(String url, String method, JsonObject body) throws InterruptedException, ExecutionException {
        var client = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder(
                        URI.create(url))
                .header("Content-Type", "application/json")
                .header("accept", "application/json")
                .version(HttpClient.Version.HTTP_1_1);

        if ( body == null ) {
            request.method(method, HttpRequest.BodyPublishers.noBody());
        }
        else {
            request.method(method, HttpRequest.BodyPublishers.ofString(body.toString()));
        }
        var responseFuture = client.sendAsync(request.build(), HttpResponse.BodyHandlers.ofString()).thenApply(HttpResponse::statusCode);

        while ( !responseFuture.isDone()) {
            Thread.sleep(20);
        }

        if ( responseFuture.get() == 200 ) {
            return true;
        }
        else {
            return false;
        }
    }

}
