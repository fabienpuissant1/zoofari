package com.cpe.Card.card.controller;

import com.cpe.Card.card.model.CardModel;
import com.cpe.Card.card.utils.http;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
public class CardDispatcher {

    private CardService cardService;

    public CardDispatcher(CardService cardService) {
        this.cardService = cardService;
    }

    public List<CardModel> getAllCards() {
        return cardService.getAllCards();
    }

    public CardModel getCard(String id) {
        return cardService.getCard(id);
    }

    public void addCard(CardModel card) {
        //Action<CardModel> action = new Action(card, "addCard");
        //producer.sendMessage(action);
        cardService.addCard(card);
    }

    public void updateCard(CardModel card) {
        //Action<CardModel> action = new Action(card, "updateCard");
        //producer.sendMessage(action);
        cardService.updateCard(card);
    }

    public void deleteCard(Long id) {
        //Action<String> action = new Action(id, "deleteCard");
        //producer.sendMessage(action);
        cardService.deleteCard(id);
    }

    public List<CardModel> getAllCardsByUserId(Long id) {
        return cardService.getAllCardsByUserId(id);
    }

    public CardModel getCardByImgUrl(String imgUrl) {
        return cardService.getCardByImgUrl(imgUrl);
    }

    public List<CardModel> getCardsByLocation(double latitude, double longitude){
        return cardService.getCardsByLocation(latitude, longitude);
    }

    public Boolean iaSuccess(CardModel cardInput) throws ExecutionException, InterruptedException {
        return cardService.iaSuccess(cardInput);
    }
    public Boolean iaFailure(CardModel cardInput) throws ExecutionException, InterruptedException {
        return cardService.iaFailure(cardInput);
    }

    public boolean lvlUp(Long userId, Long idsource, Long idtarget) throws ExecutionException, InterruptedException {
        return cardService.lvlUp(userId, idsource, idtarget);
    }

    public boolean generateCard(CardModel card) throws ExecutionException, InterruptedException {
        return cardService.generateCard(card);
    }

    public boolean findCard(CardModel card, Long userId) throws ExecutionException, InterruptedException {
        return cardService.findCard(card, userId);
    }
}
