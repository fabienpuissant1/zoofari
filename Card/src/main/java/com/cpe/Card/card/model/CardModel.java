package com.cpe.Card.card.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.JsonObject;
import org.springframework.data.jdbc.repository.query.Query;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date dateCapture;
	private double longitude;
	private double latitude;
	private int basePower;
	private int power;
	private int level;
	private Rarity rarity;
	private Long userId;
	private String imgUrl;
	private String predictedClass;
	private boolean isFound;


	public CardModel() {
		this.longitude = 0.0;
		this.latitude = 0.0;
		this.power = 0;
		this.dateCapture = new Date();
		this.rarity = Rarity.COMMON;
		this.level = 1;
		this.predictedClass = "";
		this.isFound = false;
	}

	public CardModel(double latitude, double longitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
		this.power = 0;
		this.dateCapture = new Date();
		this.rarity = Rarity.COMMON;
		this.level = 1;
		this.isFound = false;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getBasePower() {
		return basePower;
	}

	public void setBasePower(int power) {
		this.basePower = power;
	}

	public Date getDateCapture() {
		return dateCapture;
	}

	public void setDateCapture(Date dateCapture) {
		this.dateCapture = dateCapture;
	}

	public Rarity getRarity() {
		return rarity;
	}

	public void setRarity(Rarity rarity) {
		this.rarity = rarity;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getPredictedClass() {
		return predictedClass;
	}

	public void setPredictedClass(String predictedClass) {
		this.predictedClass = predictedClass;
	}

	public boolean getIsFound() {
		return isFound;
	}

	public void setIsFound(boolean found) {
		this.isFound = found;
	}

	public JsonObject toJSON() {
		JsonObject json = new JsonObject();
		json.addProperty("id", this.getId());
		json.addProperty("dateCapture", this.getDateCapture().toString());
		json.addProperty("latitude", this.getLatitude());
		json.addProperty("longitude", this.getLongitude());
		json.addProperty("basePower", this.getBasePower());
		json.addProperty("power", this.getPower());
		json.addProperty("level", this.getLevel());
		json.addProperty("rarity", this.getRarity().toString());
		json.addProperty("userId", this.getUserId());
		json.addProperty("imgUrl", this.getImgUrl());
		json.addProperty("predictedClass", this.getPredictedClass());
		json.addProperty("isFound", this.getIsFound());
		return json;
	}
}
