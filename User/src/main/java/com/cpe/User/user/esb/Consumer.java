/*package com.cpe.User.user.esb;


import com.cpe.User.user.controller.UserService;
import com.cpe.User.user.model.UserModel;
import com.model.Action;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Consumer implements ErrorHandler {

    @Autowired
    private UserService userService;

    @Autowired
    JmsTemplate jmsTemplate;

    public Consumer(UserService userservice) {
        this.userService = userservice;
    }

    @JmsListener(destination = "handle.user.action", containerFactory = "connectionFactory")
    public void receiveMessage(Action action) {
        System.err.println("Action reçue: " + action.getAction());
        if (action.getAction().equals("addUser")) {
            userService.addUser((UserModel) action.getObject());
        } else if (action.getAction().equals("updateUser")) {
            userService.updateUser((UserModel) action.getObject());
        } else if (action.getAction().equals("deleteUser")) {
            userService.deleteUser((action.getObject()).toString());
        }

    }
    @Override
    public void handleError(Throwable t) {
        log.error("Error in listener", t);
    }
}
 */