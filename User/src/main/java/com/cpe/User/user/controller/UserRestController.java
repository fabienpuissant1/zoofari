package com.cpe.User.user.controller;

import com.cpe.User.user.model.UserModel;
import org.apache.catalina.User;
import org.springframework.data.relational.core.sql.In;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {
	private static final Integer NO_USER_ID = -1;
	

	private final UserDispatcher userDispatcher;

	public UserRestController(UserDispatcher userDispatcher) {
		this.userDispatcher=userDispatcher;
	}
	
	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		List<UserModel> uDTOList=new ArrayList<UserModel>();
		for(UserModel uM: userDispatcher.getAllUsers()){
			uDTOList.add(uM);
		}
		return uDTOList;
	}
	
	@RequestMapping("/users/{id}")
	private UserModel getUser(@PathVariable String id) {

		Optional<UserModel> ruser;
		ruser= userDispatcher.getUser(Long.valueOf(id));
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/users/{id}/purchase")
	public boolean buyCard(@PathVariable String id){
		return userDispatcher.buyCard(Long.valueOf(id));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/users/{id}/canPurchase")
	public boolean canPurchase(@PathVariable String id){
		return userDispatcher.canPurchase(Long.valueOf(id));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/users/payPrice/{userId}/{gold}")
	public boolean canPurchase(@PathVariable String userId, @PathVariable String gold){
		return userDispatcher.payPrice(Long.valueOf(userId), Integer.valueOf(gold));
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public void addUser(@RequestBody UserModel user) {
		userDispatcher.addUser(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/users/updatePersonalInfo")
	public UserModel updateUser(@RequestBody UserModel user) {
		return userDispatcher.updatePersonalInfo(user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/users/addGolds")
	public UserModel addGolds(@RequestBody UserModel userModel){
		return userDispatcher.addGolds(userModel);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/users/{id}")
	public void deleteUser(@PathVariable String id) {
		userDispatcher.deleteUser(id);
	}

	@RequestMapping(method=RequestMethod.POST, value="/users/acceptFriend/{idUser}/{idFriend}")
	public boolean acceptFriend(@PathVariable String idUser, @PathVariable String idFriend) throws ExecutionException, InterruptedException {
		return userDispatcher.acceptInvite(Long.valueOf(idUser), Long.valueOf(idFriend));
	}

	@RequestMapping(method=RequestMethod.POST, value="/users/removeFriend/{idUser}/{idFriend}")
	public boolean removeFriend(@PathVariable String idUser, @PathVariable String idFriend) throws ExecutionException, InterruptedException {
		return userDispatcher.removeFriend(Long.valueOf(idUser), Long.valueOf(idFriend));
	}

	@RequestMapping(method=RequestMethod.GET, value="/users/getFriends/{idUser}")
	public Set<UserModel> getFriends(@PathVariable String idUser) {
		return userDispatcher.getFriends(Long.valueOf(idUser));
	}

	@RequestMapping(method=RequestMethod.POST, value="/users/declineInvite/{idUser}/{idFriend}")
	public boolean declineInvite(@PathVariable String idUser, @PathVariable String idFriend) throws ExecutionException, InterruptedException {
		return userDispatcher.declineInvite(Long.valueOf(idUser), Long.valueOf(idFriend));
	}

	@RequestMapping(method=RequestMethod.POST, value="/users/sendInvite/{idUser}/{idFriend}")
	public boolean sendInvite(@PathVariable String idUser, @PathVariable String idFriend) throws ExecutionException, InterruptedException {
		return userDispatcher.sendInvite(Long.valueOf(idUser), Long.valueOf(idFriend));
	}

	@RequestMapping(method=RequestMethod.GET, value="/users/getPendingInvite/{idUser}")
	public Set<UserModel> getPendingInvite(@PathVariable String idUser) {
		return userDispatcher.getPendingInvites(Long.valueOf(idUser));
	}
}
