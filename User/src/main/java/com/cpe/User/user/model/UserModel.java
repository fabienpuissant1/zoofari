package com.cpe.User.user.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.JsonObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;
	private String name;
	private String surname;
	private float golds;

	@ElementCollection
	@CollectionTable(name="friends", joinColumns=@JoinColumn(name="id"))
	@Column(name="userId")
	private Set<Long> friendList = new HashSet<>();

	@ElementCollection
	@CollectionTable(name="invite_list", joinColumns=@JoinColumn(name="id"))
	@Column(name="userId")
	private Set<Long> inviteList = new HashSet<>();

	@ElementCollection
	@CollectionTable(name="pending_list", joinColumns=@JoinColumn(name="id"))
	@Column(name="userId")
	private Set<Long> pendingList = new HashSet<>();


	public UserModel() {
		this.name = "";
		this.surname = "";
		this.golds= 0;
	}

	public UserModel(UserModel user) {
		this.userId = user.getUserId();
		this.name=user.getName();
		this.surname=user.getSurname();
		this.golds=user.getGolds();
		this.friendList=user.getFriendList();
		this.inviteList =user.getInviteList();
		this.pendingList = user.getPendingList();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public float getGolds() {
		return golds;
	}

	public void setGolds(float golds) {
		this.golds = golds;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Set<Long> getFriendList() {
		return friendList;
	}

	public void setFriendList(Set<Long> friendList) {
		this.friendList = friendList;
	}

	public void addFriend(Long idUser) {
		this.friendList.add(idUser);
	}

	public void removeFriend(Long idUser) {
		this.friendList.remove(idUser);
	}

	public Set<Long> getInviteList() {
		return inviteList;
	}

	public void addInvite(Long idUser) {
		this.inviteList.add(idUser);
	}

	public void removeInvite(Long idUser) {
		this.inviteList.remove(idUser);
	}

	public Set<Long> getPendingList() {
		return pendingList;
	}

	public void addPending(Long idUser) {
		this.pendingList.add(idUser);
	}

	public void removePending(Long idUser) {
		this.pendingList.remove(idUser);
	}



	public JsonObject toJSON() {
		JsonObject json = new JsonObject();
		json.addProperty("id", this.getId());
		json.addProperty("userId", this.getUserId());
		json.addProperty("name", this.getName());
		json.addProperty("surname", this.getSurname());
		return json;
	}

}
