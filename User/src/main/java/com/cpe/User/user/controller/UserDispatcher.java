package com.cpe.User.user.controller;

import com.cpe.User.user.model.UserModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

@Service
public class UserDispatcher {

    private UserService userService;

    public UserDispatcher(UserService userService) {
        this.userService = userService;
    }

    public List<UserModel> getAllUsers() {
        return userService.getAllUsers();
    }

    public Optional<UserModel> getUser(Long id) {
        return userService.getUser(id);
    }

    public void addUser(UserModel user) {
        //Action<UserModel> action = new Action(user, "addUser");
        //producer.sendMessage(action);
        userService.addUser(user);
    }

    public UserModel updateUser(UserModel user) {
        //Action<UserModel> action = new Action(user, "updateUser");
        //producer.sendMessage(action);
        return userService.updateUser(user);
    }


    public void deleteUser(String id) {
        //Action<String> action = new Action(id, "deleteUser");
        //producer.sendMessage(action);
        userService.deleteUser(id);
    }

    public boolean buyCard(Long id) {
        return userService.buyCard(id);
    }

    public boolean canPurchase(Long id) {
        return userService.canPurchase(id);
    }
    

    public Set<UserModel> getFriends(Long idUser) {
        return userService.getFriends(idUser);
    }

    public UserModel addGolds(UserModel userModel) {
        return userService.addGolds(userModel);
    }

    public UserModel updatePersonalInfo(UserModel user) {
        return userService.updatePersonalInfo(user);
    }

    public boolean sendInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
        return userService.sendInvite(userId, friendId);
    }

    public boolean acceptInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
        return userService.acceptInvite(userId, friendId);
    }

    public Set<UserModel> getPendingInvites(Long userId) {
        return userService.getPendingInvites(userId);
    }

    public boolean payPrice(Long userId, Integer golds) {
        return userService.payPrice(userId, golds);
    }

    public boolean declineInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
        return userService.declineInvite(userId, friendId);
    }

    public boolean removeFriend(Long userId, Long friendId) throws ExecutionException, InterruptedException {
        return userService.removeFriend(userId, friendId);
    }
}
