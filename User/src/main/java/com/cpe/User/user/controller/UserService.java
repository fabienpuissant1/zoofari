package com.cpe.User.user.controller;

import com.cpe.User.user.model.UserModel;
import com.cpe.User.user.utils.http;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class UserService {

	private final UserRepository userRepository;

	private static final String NODE_NOTIFICATION_SERVER = "http://34.196.125.97:8081/";

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(Long id) {
		return userRepository.findByUserId(id);
	}

	public void addUser(UserModel user) {
		user.setGolds(1000);
		userRepository.save(user);
	}

	public UserModel updateUser(UserModel user) {
		return userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Long.valueOf(id));
	}

    public boolean buyCard(Long id) {
    	UserModel user = userRepository.findByUserId(id).orElse(null);
    	if(user != null){
    		if(user.getGolds() >= 100) {
				user.setGolds(user.getGolds() - 100);
				userRepository.save(user);
				return true;
			}
		}
    	return false;
	}

	public boolean canPurchase(Long id) {
		UserModel user = userRepository.findByUserId(id).orElse(null);
		if(user != null){
			if(user.getGolds() > 100) {
				return true;
			}
		}
		return false;
	}

	public boolean removeFriend(Long idUser, Long idFriend) throws ExecutionException, InterruptedException {
		UserModel user = userRepository.findByUserId(idUser).get();
		UserModel friend = userRepository.findByUserId(idFriend).get();
		if (user != null) {
			user.removeFriend(idFriend);
			friend.removeFriend(idUser);
			userRepository.save(user);
			userRepository.save(friend);
			this.sendFriendNotifToServer("removeFriend", user, friend);
			return true;
		}
		return false;
	}

	public Set<UserModel> getFriends(Long id) {
		UserModel user = userRepository.findByUserId(id).get();
		HashSet<UserModel> friendList = new HashSet<>();
		if (user == null) {
			return friendList;
		}
		for (Long userId : user.getFriendList()) {
			UserModel currentFriend = userRepository.findByUserId(userId).orElse(null);
			if (currentFriend != null) {
				friendList.add(currentFriend);
			}
		}
		return friendList;
	}

	public UserModel addGolds(UserModel userModel) {
		UserModel user = this.getUser(userModel.getUserId()).orElse(null);
		if(user != null){
			user.setGolds(user.getGolds() + userModel.getGolds());
			return userRepository.save(user);
		}
		return userModel;
	}

	public UserModel updatePersonalInfo(UserModel user) {
		UserModel userModel = this.getUser(user.getUserId()).orElse(null);
		if(userModel != null){
			userModel.setName(user.getName());
			userModel.setSurname(user.getSurname());
			userRepository.save(userModel);
		}
		return userModel;
	}

	public Set<UserModel> getPendingInvites(Long id) {
		UserModel user = userRepository.findByUserId(id).get();
		HashSet<UserModel> pendingInviteList = new HashSet<>();
		if (user == null) {
			return pendingInviteList;
		}
		for (Long userId : user.getInviteList()) {
			UserModel currentFriend = userRepository.findByUserId(userId).orElse(null);
			if (currentFriend != null) {
				pendingInviteList.add(currentFriend);
			}
		}
		return pendingInviteList;
	}

	public boolean sendInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
		UserModel user = userRepository.findByUserId(userId).orElse(null);
		UserModel friend = userRepository.findByUserId(friendId).orElse(null);
		if (user != null && friend != null && !user.getFriendList().contains(friendId) && !user.getInviteList().contains(friendId) && !user.getPendingList().contains(friendId)) {
			friend.addInvite(userId);
			user.addPending(friendId);
			userRepository.save(friend);
			userRepository.save(user);
			this.sendFriendNotifToServer("sendInvite", user, friend);
			return true;
		}
		return false;
	}

	public boolean acceptInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
		UserModel user = userRepository.findByUserId(userId).orElse(null);
		UserModel friend = userRepository.findByUserId(friendId).orElse(null);
		if (user != null && friend != null && user.getInviteList().contains(friendId)) {
			user.removeInvite(friendId);
			friend.removePending(userId);
			user.addFriend(friendId);
			friend.addFriend(userId);
			userRepository.save(user);
			userRepository.save(friend);
			this.sendFriendNotifToServer("acceptFriend", user, friend);
			return true;
		}
		return false;
	}

	private void sendFriendNotifToServer(String type, UserModel user, UserModel friend) throws ExecutionException, InterruptedException {
		JsonObject json = new JsonObject();
		//userId => id of the user to notify
		//friendName => name of the sender
		json.addProperty("userId", friend.getUserId());
		json.add("friend", user.toJSON());
		http.asynchronousRequest(NODE_NOTIFICATION_SERVER + type, "POST", json);
	}

	public boolean payPrice(Long userId, Integer golds) {
		UserModel userModel = userRepository.findByUserId(userId).orElse(null);
		if(userModel != null) {
			if (userModel.getGolds() < golds) {
				return false;
			} else {
				userModel.setGolds(userModel.getGolds() - golds);
				userRepository.save(userModel);
				return true;
			}
		}
		return false;
	}

    public boolean declineInvite(Long userId, Long friendId) throws ExecutionException, InterruptedException {
		UserModel userModel = userRepository.findByUserId(userId).orElse(null);
		UserModel friend = userRepository.findByUserId(friendId).orElse(null);
		if(userModel != null){
			userModel.removeInvite(friendId);
			friend.removePending(userId);
			userRepository.save(friend);
			userRepository.save(userModel);
			this.sendFriendNotifToServer("removeFriend", userModel, friend);
			return true;
		}
		return false;
	}
}
