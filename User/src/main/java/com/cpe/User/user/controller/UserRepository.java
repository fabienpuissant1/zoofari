package com.cpe.User.user.controller;

import com.cpe.User.user.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserModel, Long> {

    Optional<UserModel> findByUserId(Long id);
}
