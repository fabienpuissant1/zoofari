class SocketService {
  socketList;

  constructor() {
    if (!SocketService.instance) {
      SocketService.instance = this;
      this.socketList = {};
    }
    return SocketService.instance;
  }

  notifyCardMinted(userId, message) {
    this.sendMessageToUser(userId, "cardMinted", message);
  }

  refreshUsers() {
    this.broadcastMessage("refreshUsers");
  }

  notifyFrontNewConnection() {
    let connectedUsers = Object.keys(this.socketList);
    this.broadcastMessage("updateConnectedUserList", connectedUsers);
  }

  sendInvite(userId, friend) {
    this.sendMessageToUser(userId, "sendInvite", friend);
  }

  acceptFriend(userId, friend) {
    this.sendMessageToUser(userId, "acceptFriend", friend);
  }

  removeFriend(userId, friend) {
    this.sendMessageToUser(userId, "removeFriend", friend);
  }

  addNewSocket(userId, socket) {
    let existingList = this.socketList[userId];
    if (existingList) {
      this.socketList[userId].push(socket);
    } else {
      this.socketList[userId] = [socket];
    }
  }

  removeSocketsByUserId(userId) {
    delete this.socketList[userId];
  }

  broadcastMessage(type, message) {
    let allKeys = Object.keys(this.socketList);
    for (let i = 0; i < allKeys.length; i++) {
      this.sendMessageToUser(allKeys[i], type, message);
    }
  }

  sendMessageToUser(userId, type, message) {
    if (this.socketList[userId] !== undefined) {
      this.socketList[userId].forEach((socket) => {
        let messageToSend = {
          type,
          message,
        };
        socket.emit("SendMessageToClient", JSON.stringify(messageToSend));
      });
    }
  }

  removeSocketBySocketId(socketId) {
    try {
      let newSocketList = this.socketList;
      let keys = Object.keys(newSocketList);
      for (let i = 0; i < keys.length; i++) {
        let socketList = newSocketList[keys[i]];
        let index = socketList.findIndex((el) => {
          return el.id === socketId;
        });
        if (index !== -1) {
          socketList.splice(index, 1);
          if (socketList.length === 0) {
            delete newSocketList[keys[i]];
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  getSocketsByUserId(userId) {
    return this.socketList[userId];
  }

  getSocketList() {
    return this.socketList;
  }

  setSocketList(newSocketList) {
    this.socketList = newSocketList;
  }
}

const instance = new SocketService();
Object.freeze(instance);

export default instance;
