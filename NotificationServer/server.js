import express from "express";
import SocketService from "./SocketService/SocketService.js";
import cors from "cors";
import http from "http";
import { Server } from "socket.io";
import bodyParser from "body-parser";

const { PORT = 8081 } = process.env;

const app = express();
app.use(express.json({ extended: false }));
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());
const server = http.createServer(app);
const ioServer = new Server(server);

ioServer.sockets.on("connection", function (socket) {
  socket.on("disconnect", (reason) => {
    SocketService.removeSocketBySocketId(socket.id);
    SocketService.notifyFrontNewConnection();
  });
});

ioServer.on("connection", (socket) => {
  socket.on("ConnectionEstablished", (userId) => {
    SocketService.addNewSocket(userId, socket);
    SocketService.notifyFrontNewConnection();
    console.log("connected : " + userId);
  });
});

app.post("/cardMinted", function (req, res) {
  if (req.body !== undefined) {
    if (req.body.userId !== undefined) {
      SocketService.notifyCardMinted(req.body.userId, req.body);
      res.sendStatus(200);
    }
  }
});

app.post("/refreshUsers", (req, res) => {
  SocketService.refreshUsers();
  res.sendStatus(200);
});

app.post("/sendInvite", (req, res) => {
  if (req.body !== undefined) {
    if (req.body.userId !== undefined && req.body.friend) {
      SocketService.sendInvite(req.body.userId, req.body.friend);
    }
  }
  res.sendStatus(200);
});

app.post("/acceptFriend", (req, res) => {
  if (req.body !== undefined) {
    if (req.body.userId !== undefined && req.body.friend) {
      SocketService.acceptFriend(req.body.userId, req.body.friend);
    }
  }
  res.sendStatus(200);
});

app.post("/removeFriend", (req, res) => {
  if (req.body !== undefined) {
    if (req.body.userId !== undefined && req.body.friend) {
      SocketService.removeFriend(req.body.userId, req.body.friend);
    }
  }
  res.sendStatus(200);
});

server.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
