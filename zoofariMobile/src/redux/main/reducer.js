export const initialUser = {
  isPhotoCurrentlyUploading: false,
  isCardMinted: false,
  cardMintedInfo: {},
  isCardMintedError: false,
  connectedUsers: [],
  toastNotification: null,
};

const reducer = (state = initialUser, action) => {
  if (action.type === "SET_IS_PHOTO_CURRENTLY_UPLOADING") {
    state = {
      ...state,
      isPhotoCurrentlyUploading: action.value,
    };
  } else if (action.type === "SET_IS_CARD_MINTED") {
    state = {
      ...state,
      isCardMinted: action.value,
    };
  } else if (action.type === "SET_CARD_MINTED_INFO") {
    state = {
      ...state,
      cardMintedInfo: action.value,
    };
  } else if (action.type === "SET_CARD_MINTED_ERROR") {
    state = {
      ...state,
      isCardMintedError: action.value,
    };
  } else if (action.type === "SET_CONNECTED_USERS") {
    state = {
      ...state,
      connectedUsers: action.value,
    };
  } else if (action.type === "SET_TOAST_NOTIFICATION") {
    state = {
      ...state,
      toastNotification: action.value,
    };
  } else {
    return state;
  }

  return state;
};

export default reducer;
