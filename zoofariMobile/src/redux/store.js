import { createStore, applyMiddleware, compose } from "redux";
import { combineReducers } from "redux";
import thunk from "redux-thunk";

import userReducer from "./user/reducer";
import mainReducer from "./main/reducer";

const combineReducer = combineReducers({
  userState: userReducer,
  mainState: mainReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  combineReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
