export const initialUser = {
  name: "",
  surname: "",
  golds: 0,
  frienList: [],
  inviteList: [],
  pendingList: [],
};

const reducer = (state = initialUser, action) => {
  if (action.type === "SET_USER") {
    state = action.value;
  } else if (action.type === "UPDATE_PERSONAL_INFO") {
    state = {
      ...state,
      name: action.value.name,
      surname: action.value.surname,
    };
  } else if (action.type === "UPDATE_GOLDS") {
    state = {
      ...state,
      golds: action.value,
    };
  } else {
    return state;
  }

  return state;
};

export default reducer;
