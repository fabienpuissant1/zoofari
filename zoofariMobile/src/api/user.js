import AsyncStorage from "@react-native-async-storage/async-storage";
import store from "../redux/store";

export const getUser = async () => {
  try {
    const value = await AsyncStorage.getItem("user");
    if (value !== null) {
      return JSON.parse(value);
    }
  } catch (e) {
    return null;
  }
};

export const setUser = async (user) => {
  try {
    await AsyncStorage.setItem("user", JSON.stringify(user));
    store.dispatch(setUserInStore(user));
  } catch (e) {
    return null;
  }
};

const setUserInStore = (user) => {
  return {
    type: "SET_USER",
    value: user,
  };
};
