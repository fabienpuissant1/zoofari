export const groubByDistance = (cardsParam) => {
  let cards = [...cardsParam];
  let newList = [[cards[0]]];
  let founded = false;
  for (let i = 1; i < cards.length; i++) {
    for (let j = 0; j < newList.length; j++) {
      founded = false;
      let cardToAnalyze = cards[i];
      let cardGrouped = newList[j][0];
      if (
        distanceBetweenTwoCoords(
          cardToAnalyze.latitude,
          cardToAnalyze.longitude,
          cardGrouped.latitude,
          cardGrouped.longitude
        ) < 20
      ) {
        newList[j].push(cardToAnalyze);
        founded = true;
        break;
      }
    }
    if (!founded) {
      newList.push([cards[i]]);
    }
  }
  return newList;
};

const distanceBetweenTwoCoords = (lat1, lon1, lat2, lon2) => {
  var R = 6371; // km
  var dLat = toRad(lat2 - lat1);
  var dLon = toRad(lon2 - lon1);
  var lat1 = toRad(lat1);
  var lat2 = toRad(lat2);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d * 1000;
};
const toRad = (Value) => {
  return (Value * Math.PI) / 180;
};
