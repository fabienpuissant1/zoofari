import { mockSuccess, mockFailure } from "./utils";

export const loginUser = (email, password) => {
  if (email !== "admin" && email !== "Admin") {
    return mockFailure({ error: 500, message: "Something went wrong!" });
  }
  return mockSuccess({ auth_token: "successful_fake_token" });
};

export const createAccount = (email, password) => {
  if (email === "admin" || email === "Admin") {
    return mockFailure({ error: 500, message: "Something went wrong!" });
  }

  return mockSuccess({ message: "user created" });
};

export const recoverAccount = (email) => {
  if (email === "admin" || email === "Admin") {
    return mockSuccess({ message: "success" });
  }
  return mockFailure({ error: 500, message: "Something went wrong!" });
};
