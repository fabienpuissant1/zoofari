import { mockSuccess, mockFailure } from "./utils";

export const getUserCards = (userId) => {
  return mockSuccess([
    {
      id: 1,
      power: 10,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 1,
      cardOverlay: require("../../assets/cardRessources/OneStar.png"),
      artwork:
        "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.6435-9/116071340_4378116262206205_3049805834704245872_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=e1FIfWk_uSUAX_6UuXR&_nc_oc=AQn_wT-stbiGPid60wML5F2prqgwnCddOnZQAeF3l6S15vqJpx0M02APXg6YcUD-RwY&_nc_ht=scontent-cdt1-1.xx&oh=00_AT8gCc-MV3ZCrKAuvhVHPmokYPNIpFYwd9ai1ltM7AW1BQ&oe=61FB71B2",
    },
    {
      id: 2,
      power: 20,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 2,
      cardOverlay: require("../../assets/cardRessources/TwoStar.png"),
      artwork:
        "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.6435-9/73238916_3125383097536885_6575442725124964352_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=8tpkrRSBz-EAX8L5kn6&_nc_ht=scontent-cdt1-1.xx&oh=00_AT_pc5x6gV_aoZHl-uAZdmHGJu0gLY28aSobemp1nLzr-g&oe=61FC5E06",
    },
    {
      id: 3,
      power: 20,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 2,
      cardOverlay: require("../../assets/cardRessources/ThreeStar.png"),
      artwork:
        "https://img-4.linternaute.com/JCvRDi8diTuc6e5r0nPCKvI4trY=/1500x/smart/09804d2b389842f8817cfef31a93c4f4/ccmcms-linternaute/1784319.jpg",
    },
    {
      id: 4,
      power: 20,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 2,
      cardOverlay: require("../../assets/cardRessources/FourStar.png"),
      artwork:
        "https://img-4.linternaute.com/JCvRDi8diTuc6e5r0nPCKvI4trY=/1500x/smart/09804d2b389842f8817cfef31a93c4f4/ccmcms-linternaute/1784319.jpg",
    },
    {
      id: 5,
      power: 20,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 2,
      cardOverlay: require("../../assets/cardRessources/FiveStar.png"),
      artwork:
        "https://img-4.linternaute.com/JCvRDi8diTuc6e5r0nPCKvI4trY=/1500x/smart/09804d2b389842f8817cfef31a93c4f4/ccmcms-linternaute/1784319.jpg",
    },
    {
      id: 6,
      power: 20,
      name: "Card1",
      img: "https://images-eu.ssl-images-amazon.com/images/I/51bjI0YcXbL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
      basePower: 10,
      level: 2,
      cardOverlay: require("../../assets/cardRessources/SixStar.png"),
      artwork:
        "https://scontent.flyn1-1.fna.fbcdn.net/v/t1.6435-9/51112737_2238117872885529_5204727410372640768_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=vh3dQJRjh0cAX8HWn7A&_nc_ht=scontent.flyn1-1.fna&oh=00_AT-0_9saINveMrJjC5kVN4HpYCgP5GWCJpms9mgPA8BJdw&oe=61FCD4A7",
    },
  ]);
};
