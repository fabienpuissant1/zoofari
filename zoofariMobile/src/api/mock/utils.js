export const mockSuccess = (value) => {
	return new Promise((resolve) => {
		setTimeout(() => resolve(value), 100);
	});
};

export const mockFailure = (value) => {
	return new Promise((resolve, reject) => {
		setTimeout(() => reject(value), 100);
	});
};
