import * as AuthService from "./authentificationService";
import * as UserService from "./userService";
import * as cardService from "./cardService";

export class Api {
  authService = {
    loginUser: (email, password) => AuthService.loginUser(email, password),
    createAccount: (email, password) =>
      AuthService.createAccount(email, password),
    recoverAccount: (email) => AuthService.recoverAccount(email),
  };

  userService = {
    getUserInfo: (email) => UserService.getUserInfo(email),
    updateEmail: (newParam, success) =>
      UserService.updateEmail(newParam, success),
    updatePassword: (newParam, success) =>
      UserService.updatePassword(newParam, success),
  };

  cardService = {
    getUserCards: (userId) => cardService.getUserCards(userId),
  };
}
