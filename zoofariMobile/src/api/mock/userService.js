import { mockSuccess, mockFailure } from "./utils";

export const getUserInfo = async () => {
  return mockSuccess({
    email: "fabienpuissant@live.fr",
    firstName: "Fabien",
    surName: "Puissant",
    phoneNumber: {
      value: "638616084",
      formattedValue: "+33638616084",
    },
    Addresses: [
      {
        firstLine: "2 rue de la capsulerie",
        secondLine: "93170 Bagnolet",
        city: "Paris",
        zipCode: "75010",
        used: true,
        isBillingAddress: true,
      },
      {
        firstLine: "2 rue de la bastille",
        secondLine: "",
        city: "Paris",
        zipCode: "75010",
        used: false,
        isBillingAddress: false,
      },
    ],
  });
};

export const updateEmail = (newParams, success = true) => {
  if (success) {
    return mockSuccess();
  } else {
    return mockFailure({ error: 500, message: "server issue" });
  }
};

export const updatePassword = (newParams, success = true) => {
  if (success) {
    return mockSuccess();
  } else {
    return mockFailure({ error: 500, message: "server issue" });
  }
};
