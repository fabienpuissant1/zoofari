import axios from "axios";
import { NodeS3Url } from "../serverUrl";

export const sendImageToServer = async (photo) => {
  //photo: { base64: , name: }
  let res = await axios.post(NodeS3Url + "sendPhoto", photo);
  return res.data;
};
