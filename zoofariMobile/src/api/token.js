import AsyncStorage from "@react-native-async-storage/async-storage";
import { setAuthorizationToken } from "./authorizationHeaders";
import jwt_decode from "jwt-decode";
import { Api } from "../api";
import store from "../redux/store";

export const getToken = async () => {
  try {
    const value = await AsyncStorage.getItem("@auth_token");
    if (value !== null) {
      return value;
    }
  } catch (e) {
    return null;
  }
};

export const setToken = async (token) => {
  try {
    await AsyncStorage.setItem("@auth_token", token);
  } catch (e) {
    return null;
  }
};

export const checkTokenAndDispatch = async (token = null) => {
  if (!token) {
    token = await getToken();
  }
  try {
    const data = jwt_decode(token);
    if (data !== null) {
      if (data.exp > Math.round(new Date().getTime() / 1000)) {
        setAuthorizationToken(token);
        return true;
      } else {
        setToken("");
        return false;
      }
    }
  } catch {
    return false;
  }
};

export const getUserInfoFromToken = async () => {
  let token = await getToken();
  if (token) {
    const data = jwt_decode(token);
    let res = await Api.userService.getUserById(data.sub);
    return res;
  }
};

export const getFriendList = async (userId) => {
  let friendList = await Api.userService.getFriendList(userId);
  return friendList;
};

export const refreshUser = async () => {
  let userData = await getUserInfoFromToken();
  if (userData !== undefined) {
    store.dispatch({
      type: "SET_USER",
      value: userData,
    });
  }
};
