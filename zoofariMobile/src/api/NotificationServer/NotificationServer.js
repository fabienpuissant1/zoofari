import store from "../../redux/store";
import { io } from "socket.io-client";
import { notificationServerUrl } from "../serverUrl";
import { formatCardInfo } from "../final/cardService";
import { refreshUser } from "../../api/token";

class NotificationServer {
  socket;

  constructor() {
    if (!NotificationServer.instance) {
      NotificationServer.instance = this;
      this.socket = io(notificationServerUrl, {
        reconnection: true,
        origins: "*",
        transports: ["websocket"],
      });
      this.socket.on("SendMessageToClient", (message) => {
        console.log(message);
        let data = JSON.parse(message);
        if (data.type === "cardMinted") {
          this.photoUploaded(message);
        } else if (data.type === "refreshUsers") {
          refreshUser();
        } else if (data.type === "sendInvite") {
          let fullName = this.getFullName(data.message);
          let message = "Friend request from " + fullName;
          store.dispatch({
            type: "SET_TOAST_NOTIFICATION",
            value: {
              title: message,
              status: "info",
            },
          });
          refreshUser();
        } else if (data.type === "acceptFriend") {
          let fullName = this.getFullName(data.message);
          let message = "Friend accepted " + fullName;
          store.dispatch({
            type: "SET_TOAST_NOTIFICATION",
            value: {
              title: message,
              status: "success",
            },
          });
          refreshUser();
        } else if (data.type === "removeFriend") {
          let fullName = this.getFullName(data.message);
          let message = fullName + " unfriended you";
          store.dispatch({
            type: "SET_TOAST_NOTIFICATION",
            value: {
              title: message,
              status: "error",
            },
          });
          refreshUser();
        } else if (data.type === "updateConnectedUserList") {
          let parsedData = [];
          data.message.forEach((el) => {
            parsedData.push(parseInt(el));
          });
          store.dispatch({
            type: "SET_CONNECTED_USERS",
            value: parsedData,
          });
        }
      });
    }
    return NotificationServer.instance;
  }

  getFullName(user) {
    let fullName = "";
    if (user.name !== undefined) {
      fullName += user.name + " ";
    }
    if (user.surname !== undefined) {
      fullName += user.surname;
    }
    return fullName;
  }

  connectUser(userId) {
    this.socket.connect();
    this.socket.emit("ConnectionEstablished", userId);
  }

  disconnectUser() {
    this.socket.disconnect();
  }

  photoUploaded(message) {
    store.dispatch({
      type: "SET_IS_PHOTO_CURRENTLY_UPLOADING",
      value: false,
    });
    let data = JSON.parse(message);
    let card = formatCardInfo(data.message);
    if (card.fail === undefined) {
      store.dispatch({
        type: "SET_CARD_MINTED_INFO",
        value: card,
      });
      store.dispatch({
        type: "SET_IS_CARD_MINTED",
        value: true,
      });
      refreshUser();
    } else {
      store.dispatch({
        type: "SET_CARD_MINTED_ERROR",
        value: true,
      });
    }
  }
}

const instance = new NotificationServer();
Object.freeze(instance);

export default instance;
