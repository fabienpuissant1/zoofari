import * as AuthService from "./authentificationService";
import * as UserService from "./userService";
import * as CardService from "./cardService";

export class Api {
  authService = {
    loginUser: (email, password) => AuthService.loginUser(email, password),
    createAccount: (email, password, name, surname) =>
      AuthService.createAccount(email, password, name, surname),
    recoverAccount: (email) => AuthService.recoverAccount(email),
    updateEmail: (email) => AuthService.updateEmail(email),
    updatePassword: (newParam) => AuthService.updatePassword(newParam),
  };

  userService = {
    getUserById: (userId) => UserService.getUserById(userId),
    getFriendList: (userId) => UserService.getFriendList(userId),
    getAllUsers: () => UserService.getAllUsers(),
    getPendingInvite: (userId) => UserService.getPendingInvite(userId),
    sendInvite: (userId, friendId) => UserService.sendInvite(userId, friendId),
    acceptFriend: (userId, friendId) =>
      UserService.acceptFriend(userId, friendId),
    declineFriend: (userId, friendId) =>
      UserService.declineFriend(userId, friendId),
    removeFriend: (userId, friendId) =>
      UserService.removeFriend(userId, friendId),
  };

  cardService = {
    buyCard: (params) => CardService.buyCard(params),
    getUserCards: (userId) => CardService.getUserCards(userId),
    levelUpCards: (userId, cardId1, cardId2) =>
      CardService.levelUpCards(userId, cardId1, cardId2),
    getCardsArround: (lat, long) => CardService.getCardsArround(lat, long),
    findCard: (card, userId) => CardService.findCard(card, userId),
  };
}
