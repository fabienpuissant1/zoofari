import axios from "axios";
import { authServerUrl } from "../serverUrl";

const baseUrl = authServerUrl + "api/auth/";

export const loginUser = async (email, password) => {
  let res = await axios.post(baseUrl + "signin", { email, password });
  return res.data;
};

export const createAccount = async (email, password, name, surname) => {
  let res = await axios.post(baseUrl + "signup", {
    email,
    password,
    name,
    surname,
    role: ["user"],
  });
  return res.data;
};

export const recoverAccount = (email) => {
  if (email === "admin" || email === "Admin") {
    return mockSuccess({ message: "success" });
  }
  return mockFailure({ error: 500, message: "Something went wrong!" });
};

const baseInfoUrl = authServerUrl + "api/UserInfoService/";
const baseUserUrl = authServerUrl + "api/UserService/";

export const getUserInfo = async () => {
  let res = await axios.get(baseInfoUrl + "getUserInfo");
  return res.data;
};

/*param
    string
*/
export const updateEmail = async (email) => {
  let res = await axios.put(baseUserUrl + "updateEmail", { email });
  return res.data;
};

/*param
{
    "oldPassword": "String",
    "password":  "String",
}
*/
export const updatePassword = async (newParams) => {
  let res = await axios.put(baseUserUrl + "updatePassword", newParams);
  return res.data;
};
