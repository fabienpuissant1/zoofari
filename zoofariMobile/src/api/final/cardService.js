import axios from "axios";
import { cardServerUrl } from "../serverUrl";

export const buyCard = async (params) => {
  let res = await axios.post(cardServerUrl + "cards/generateCard", params);
  return res.data;
};

export const getUserCards = async (userId) => {
  let res = await axios.get(cardServerUrl + "cards/user/" + userId);
  let formattedCards = reformatCards(res.data);
  return formattedCards;
};

export const getCardsArround = async (lat, long) => {
  let res = await axios.get(cardServerUrl + "cards/" + lat + "/" + long);
  return res.data;
};

export const levelUpCards = async (userId, cardId1, cardId2) => {
  let res = await axios.post(
    cardServerUrl + "cards/lvlup/" + userId + "/" + cardId1 + "/" + cardId2
  );
  return res.data;
};

export const findCard = async (card, userId) => {
  let res = await axios.post(cardServerUrl + "cards/findCard/" + userId, card);
  return res.data;
};

const reformatCards = (cardList) => {
  let cardListFormatted = [];
  for (let i = 0; i < cardList.length; i++) {
    let formatCard = formatCardInfo(cardList[i]);
    cardListFormatted.push(formatCard);
  }
  return cardListFormatted;
};

export const formatCardInfo = (card) => {
  let formattedCard = { ...card };
  formattedCard.artwork = formattedCard.imgUrl;
  formattedCard.cardOverlay = cardOverlays[formattedCard.rarity];
  return formattedCard;
};

export const formatCardList = (cardList) => {
  let formattedList = [];
  for (let i = 0; i < cardList.length; i++) {
    let cardFormatted = formatCardInfo(cardList[i]);
    formattedList.push(cardFormatted);
  }
  return formattedList;
};

const cardOverlays = {
  COMMON: require("../../assets/cardRessources/OneStar.png"),
  UNCOMMON: require("../../assets/cardRessources/TwoStar.png"),
  RARE: require("../../assets/cardRessources/ThreeStar.png"),
  EPIC: require("../../assets/cardRessources/FourStar.png"),
  LEGENDARY: require("../../assets/cardRessources/FiveStar.png"),
  GODLIKE: require("../../assets/cardRessources/SixStar.png"),
};

export const isUpgradable = (card) => {
  return true;
};

export const isSacrifiable = (cardToLevelUp, cardToSacrify) => {
  //If COMMON and COMMON -> return true
  if (cardToLevelUp.level === cardToSacrify.level) {
    let rarityIndex = cardRaritiesArray.findIndex((el) => {
      return el === cardToLevelUp.rarity;
    });
    if (rarityIndex !== -1 && rarityIndex >= 1) {
      if (cardToSacrify.rarity === cardRaritiesArray[rarityIndex - 1]) {
        return true;
      }
    }
    if (
      cardToLevelUp.rarity === "COMMON" &&
      cardToSacrify.rarity === "COMMON"
    ) {
      return true;
    }
  }
  return false;
};

export const cardRaritiesArray = [
  "COMMON",
  "UNCOMMON",
  "RARE",
  "EPIC",
  "LEGENDARY",
  "GODLIKE",
];
