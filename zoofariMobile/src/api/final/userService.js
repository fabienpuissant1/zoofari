import axios from "axios";
import { userServerUrl } from "../serverUrl";

export const getUserById = async (userId) => {
  let res = await axios.get(userServerUrl + "users/" + userId);
  return res.data;
};

export const getFriendList = async (userId) => {
  let res = await axios.get(userServerUrl + "users/getFriends/" + userId);
  return res.data;
};

export const getAllUsers = async () => {
  let res = await axios.get(userServerUrl + "users");
  return res.data;
};

export const getPendingInvite = async (userId) => {
  let res = await axios.get(userServerUrl + "users/getPendingInvite/" + userId);
  return res.data;
};

export const sendInvite = async (userId, friendId) => {
  let res = await axios.post(
    userServerUrl + "users/sendInvite/" + userId + "/" + friendId
  );
  return res.data;
};

export const acceptFriend = async (userId, friendId) => {
  let res = await axios.post(
    userServerUrl + "users/acceptFriend/" + userId + "/" + friendId
  );
  return res.data;
};

export const declineFriend = async (userId, friendId) => {
  let res = await axios.post(
    userServerUrl + "users/declineInvite/" + userId + "/" + friendId
  );
  return res.data;
};

export const removeFriend = async (userId, friendId) => {
  let res = await axios.post(
    userServerUrl + "users/removeFriend/" + userId + "/" + friendId
  );
  return res.data;
};
