export const isNOE = (value) => {
  if (value !== undefined) {
    if (value !== null) {
      if (value !== "") {
        return false;
      }
    }
  }
  return true;
};

export const addressToString = (address, short = null) => {
  let addressString = "";
  if (address.firstLine !== undefined) {
    addressString += address.firstLine;
  }
  if (address.secondLine !== undefined) {
    addressString += " " + address.secondLine;
  }
  if (address.city !== undefined) {
    addressString += " " + address.city;
  }
  if (short) {
    if (addressString.length > short) {
      return addressString.substring(0, short) + "...";
    }
  }
  return addressString;
};
