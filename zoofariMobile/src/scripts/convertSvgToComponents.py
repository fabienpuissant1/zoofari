import argparse
import os
import glob

def find_and_upper_next(l, arg, upperIndex):
    while l.find(arg) != -1:
        i = l.find(arg)
        l = l[: i + upperIndex] + l[i + upperIndex].upper() + l[upperIndex + i + 1:]
    return l

def replace_svg_declaration(l):
    index = l.find("viewBox")
    if index != -1:
        return "<Svg height={height} width={width} " + l[index:] 

def replace_file_content(filedir):
    with open(filedir,"r") as f:
        lines = f.readlines()

    #Getting the name
    index = filedir.find("\\")
    name = filedir[index+1:-4]

    #adding import
    out = "import React from 'react' \nimport Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; \n\n"
    out += "export default function " + name + "Icon({height = 40, width = 40}) {\n"
    out += "\treturn (\n"

                        
    #Convert babel-case to camelCase
    for l in lines:
        if l[:6] != "import":

            if l.find("<svg") != -1:
                l = replace_svg_declaration(l)

            while l.find("-") != -1:
                i = l.find("-")
                if i != -1:
                    l = l[:i] + l[i+1].upper() + l[i+2:]
            
            l = find_and_upper_next(l, "</svg", 2)
            l = find_and_upper_next(l, "<g", 1)
            l = find_and_upper_next(l, "</g", 2)
            l = find_and_upper_next(l, "<path", 1)
            l = find_and_upper_next(l, "</path", 2)
            l = find_and_upper_next(l, "<defs", 1)
            l = find_and_upper_next(l, "</defs", 2)
            l = find_and_upper_next(l, "<clipPath", 1)
            l = find_and_upper_next(l, "</clipPath", 2)
            l = find_and_upper_next(l, "<rect", 1)
            l = find_and_upper_next(l, "<mask", 1)
            l = find_and_upper_next(l, "</mask", 2)
            l = find_and_upper_next(l, "<filter", 1)
            l = find_and_upper_next(l, "</filter", 2)
        out += l

    out += "\n)\n"
    out += "}"
    with open("../assets/icons/iconComponents/" + name + "Icon.js", "w") as f:
        f.writelines(out)




def main():
    # if os.path.isfile(filedir):
    #     replace_file_content(filedir)
    # elif os.path.isdir(filedir):
    files = glob.glob("../assets/icons/figmaExport/*")
    for file in files:
        print(file)
        replace_file_content(file)

main()

# if __name__ =="__main__":
#     parser = argparse.ArgumentParser()
#     parser.add_argument("filedir",help="Filedir")
#     args = parser.parse_args()

#     main(args.filedir)