import { Box, ScrollView, View, Center, useToast } from "native-base";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text } from "react-native";
import OrangeButton from "../Buttons/OrangeButton";
import EmailInput from "../Inputs/EmailInput";
import PasswordInput from "../Inputs/PasswordInput";
import colors from "../../assets/colors/colors";
import { isNOE } from "../../utils";
import {
  checkTokenAndDispatch,
  setToken,
  getUserInfoFromToken,
} from "../../api/token";
import NotificationServer from "../../api/NotificationServer/NotificationServer";
import fontStyles from "../../assets/fontStyles/fontStyles";
import LogoIcon from "../../assets/icons/IconComponents/LogoIcon";
import { setUser } from "../../api/user";
import { Api } from "../../api";
import { initialUser } from "../../redux/user/reducer";
import { useDispatch } from "react-redux";

export default function LoginPage({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();
  const dispatch = useDispatch();
  const id = "identification-toast";

  useEffect(() => {
    setToken("");
    setUser(initialUser);
  }, []);

  const login = () => {
    if (!isNOE(email) && !isNOE(password)) {
      setIsLoading(true);
      Api.authService
        .loginUser(email, password)
        .then(async (res) => {
          setIsLoading(false);
          await setToken(res.accessToken);
          await checkTokenAndDispatch(res.accessToken);
          let userData = await getUserInfoFromToken();
          if (userData !== undefined) {
            dispatch({
              type: "SET_USER",
              value: userData,
            });
            NotificationServer.connectUser(userData.userId);
          }
          setEmail("");
          setPassword("");
          resetForm();
          navigation.navigate("Home");
        })
        .catch((err) => {
          console.log("error :", err.message);
          if (!toast.isActive(id)) {
            toast.show({
              id,
              title: "Wrong credentials",
              status: "error",
              placement: "top",
            });
          }
          resetForm();
          setIsLoading(false);
        });
    }
  };

  const navigateCreateAccount = () => {
    navigation.navigate("CreateAccount");
  };

  const handleEmailChanged = (value) => {
    setEmail(value);
  };

  const handlePasswordChanged = (value) => {
    setPassword(value);
  };

  const resetForm = () => {
    setEmail("");
    setPassword("");
  };

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <Center style={styles.logoContainer}>
          <LogoIcon width={34} height={41} />
        </Center>

        <Center>
          <Text style={[styles.loginText, fontStyles.h1]}>Login</Text>
        </Center>

        <View style={styles.emailInput}>
          <EmailInput
            placeholder="Email"
            title="Email"
            value={email}
            onValueChanged={handleEmailChanged}
          />
        </View>

        <View>
          <PasswordInput
            placeholder="Password"
            title="Password"
            value={password}
            onValueChanged={handlePasswordChanged}
          />
        </View>

        <View style={{ marginTop: 20 }}>
          <OrangeButton
            label="Login"
            onClick={login}
            isLoading={isLoading}
            isLoadingText="Loading"
          />
        </View>

        <Center>
          <Text style={[styles.dontHaveAccountText, fontStyles.p2]}>
            No account yet ?
          </Text>
        </Center>

        <Center>
          <Text
            style={[styles.createAccountText, fontStyles.h3]}
            onPress={navigateCreateAccount}
          >
            Create an account
          </Text>
        </Center>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  mainContainer: {
    marginTop: 5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 10,
  },
  logoContainer: {
    marginTop: 24,
    marginBottom: 16,
  },
  logoImage: {
    height: 41,
    width: 34,
  },
  loginText: {
    color: colors.grey8,
    marginBottom: 8,
  },
  loginDescription: {
    color: colors.grey7,
    textAlign: "center",
    marginBottom: 24,
  },
  emailInput: {
    marginBottom: 16,
  },
  orText: {
    color: colors.grey6,
    marginTop: 8,
    marginBottom: 8,
  },
  loginFacebook: {
    marginBottom: 8,
  },
  dontHaveAccountText: {
    color: colors.grey7,
    marginTop: 24,
  },
  createAccountText: {
    color: colors.purple5,
    fontSize: 16,
  },
  alertContainer: {
    marginTop: 10,
  },
});
