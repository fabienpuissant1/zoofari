import { Box, ScrollView, View, Center, useToast } from "native-base";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text } from "react-native";
import OrangeButton from "../Buttons/OrangeButton";
import EmailInput from "../Inputs/EmailInput";
import PasswordInput from "../Inputs/PasswordInput";
import colors from "../../assets/colors/colors";
import { isNOE } from "../../utils";
import LogoIcon from "../../assets/icons/IconComponents/LogoIcon";
import fontStyles from "../../assets/fontStyles/fontStyles";
import { Api } from "../../api";
import TextInput from "../Inputs/TextInput";

export default function CreateAccountPage({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [surName, setSurName] = useState("");

  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordInvalid, setIsPasswordInvalid] = useState(false);
  const [isConfirmPasswordInvalid, setIsConfirmPasswordInvalid] =
    useState(false);
  const toast = useToast();
  const id = "identification-toast";

  const create = () => {
    if (
      !isNOE(email) &&
      !isNOE(password) &&
      !isNOE(firstName) &&
      !isNOE(surName)
    ) {
      if (!isPasswordInvalid && !isConfirmPasswordInvalid) {
        setIsLoading(true);
        Api.authService
          .createAccount(email, password, firstName, surName)
          .then((res) => {
            toast.show({
              id,
              title: "Account created",
              status: "success",
              placement: "top",
            });
            resetForm();
            setIsLoading(false);
            navigateLogin();
          })
          .catch((err) => {
            console.log(err);
            if (!toast.isActive(id)) {
              toast.show({
                id,
                title: "Email already in use",
                status: "error",
                placement: "top",
              });
            }
            resetForm();
            setIsLoading(false);
          });
      }
    }
  };

  const resetForm = () => {
    setPassword("");
    setEmail("");
    setConfirmPassword("");
    setSurName("");
    setFirstName("");
  };

  const handleChangeFirstName = (value) => {
    setFirstName(value);
  };

  const handleChangeSurName = (value) => {
    setSurName(value);
  };

  const navigateLogin = () => {
    navigation.navigate("Login");
  };

  const handleEmailChanged = (value) => {
    setEmail(value);
  };

  const handlePasswordChanged = (value) => {
    setPassword(value);
  };

  const handleConfirmPasswordChanged = (value) => {
    setConfirmPassword(value);
  };

  useEffect(() => {
    if (password === "") {
      setIsPasswordInvalid(false);
    } else if (password.length > 7) {
      setIsPasswordInvalid(false);
    } else {
      setIsPasswordInvalid(true);
    }
  }, [password]);

  useEffect(() => {
    if (confirmPassword === "") {
      setIsConfirmPasswordInvalid(false);
    } else if (confirmPassword === password) {
      setIsConfirmPasswordInvalid(false);
    } else {
      setIsConfirmPasswordInvalid(true);
    }
  }, [confirmPassword, password]);

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <Center style={styles.logoContainer}>
          <LogoIcon width={34} height={41} />
        </Center>

        <Center>
          <Text style={[styles.loginText, fontStyles.h1]}>
            Create an account
          </Text>
        </Center>

        <View style={styles.emailInput}>
          <EmailInput
            placeholder="Email"
            title="Email"
            value={email}
            onValueChanged={handleEmailChanged}
          />
        </View>

        <View>
          <PasswordInput
            placeholder="Password"
            title="Password"
            value={password}
            isInvalid={isPasswordInvalid}
            onValueChanged={handlePasswordChanged}
          />
        </View>

        <View>
          <Text
            style={[
              styles.rulePasswordText,
              { color: isPasswordInvalid ? colors.red5 : colors.grey5 },
              fontStyles.p4,
            ]}
          >
            At least 8 characters needed
          </Text>
        </View>

        <View>
          <PasswordInput
            placeholder="Password"
            title="Confirm password"
            value={confirmPassword}
            isInvalid={isConfirmPasswordInvalid}
            onValueChanged={handleConfirmPasswordChanged}
          />
        </View>

        {isConfirmPasswordInvalid && (
          <View>
            <Text
              style={[
                styles.rulePasswordText,
                { color: colors.red5 },
                fontStyles.p4,
              ]}
            >
              passwords don't match
            </Text>
          </View>
        )}

        <View style={styles.margin}>
          <TextInput
            title="Firstname"
            value={firstName}
            onValueChanged={handleChangeFirstName}
            placeholder="Firstname"
          />
        </View>

        <View style={styles.margin}>
          <TextInput
            title="Lastname"
            value={surName}
            onValueChanged={handleChangeSurName}
            placeholder="Lastname"
          />
        </View>

        <View style={{ marginTop: 20 }}>
          <OrangeButton
            label="Create account"
            onClick={create}
            isLoading={isLoading}
            isLoadingText="Creating"
          />
        </View>

        <Center>
          <Text style={[styles.dontHaveAccountText, fontStyles.p2]}>
            Already created?
          </Text>
        </Center>

        <Center>
          <Text
            style={[styles.createAccountText, fontStyles.h3]}
            onPress={navigateLogin}
          >
            Login
          </Text>
        </Center>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  mainContainer: {
    marginTop: 5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 40,
  },
  logoContainer: {
    marginTop: 24,
    marginBottom: 16,
  },
  logoImage: {
    height: 41,
    width: 34,
  },
  loginText: {
    color: colors.grey8,
    marginBottom: 8,
  },
  loginDescription: {
    color: colors.grey7,
    textAlign: "center",
    marginBottom: 24,
  },
  emailInput: {
    marginBottom: 16,
  },
  orText: {
    color: colors.grey6,
    marginTop: 8,
    marginBottom: 8,
  },
  loginFacebook: {
    marginBottom: 8,
  },
  dontHaveAccountText: {
    color: colors.grey7,
    marginTop: 24,
  },
  createAccountText: {
    color: colors.purple5,
  },
  alertContainer: {
    marginTop: 10,
  },
  rulePasswordText: {
    marginTop: 8,
    marginBottom: 16,
    marginLeft: 4,
  },
  margin: {
    marginTop: 15,
  },
});
