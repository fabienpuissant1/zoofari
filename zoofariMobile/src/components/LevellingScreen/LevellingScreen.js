import React, { useState, useEffect } from "react";
import { View, Box } from "native-base";
import { Api } from "../../api";
import Card from "../CollectionScreen/Card";
import { FlatList, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";

export default function LevellingScreen({cardToLevelUp}){
    const [cardlist, setCardList] = useState([]);
    const isFocused = useIsFocused();
    var reducedCardList = [];

    //Function to remove card not fitting requirements (i.e. wrong level/rarity)
    function reduceCardList(){
      for(let i = 0; i < len(cardlist); i++){
          //conditions respecting game principles
          if(cardlist[i].level == cardToLevelUp.level && 
            ((cardToLevelUp.rarity == EPIC && cardlist[i].rarity == RARE) || 
            (cardToLevelUp.rarity == LEGENDARY && cardlist[i].rarity == EPIC) || 
            (cardToLevelUp.rarity == GODLIKE && cardlist[i].rarity == LEGENDARY))){
              reducedCardList.add(cardlist[i])
          }
      }
    }

    const fetchCards = async () => {
      if (user.userId !== undefined) {
        let res = await Api.cardService.getUserCards(user.userId);
        setCardList(res);
        reduceCardList(); //Setting up smaller cardlist when view is displayed
      }
    };
  
    useEffect(() => {
      if (isFocused) {
        fetchCards();
      }
    }, [isFocused]);
    

    return (
        <Box safearea style={{ marginBottom: 100 }}>
          <FlatList
            data={reducedCardList}
            renderItem={({ item }) => {
              if(cardToLevelUp.level == item.level){
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    margin: 5,
                  }}
                >
                  <Card data={item} key={item.id} />
                </View>
              }
            }}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
          />
        </Box>
    );
}