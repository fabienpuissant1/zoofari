import { View } from "native-base";
import React from "react";
import { ScrollView, StyleSheet, ImageBackground, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import CardsIcon from "../../assets/icons/IconComponents/CardsIcon";
import HidePasswordIcon from "../../assets/icons/IconComponents/HidePasswordIcon";

export default function MenuScreen({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView horizontal style={styles.scrollView}>
        <View style={styles.viewContainer}>
          <ImageBackground
            style={styles.img}
            source={{
              uri: "https://images.alphacoders.com/672/672035.jpg",
            }}
          >
            <View style={styles.cards}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Collection");
                }}
              >
                <HidePasswordIcon></HidePasswordIcon>
              </TouchableOpacity>
            </View>
            <View style={styles.lvlup}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Friends");
                }}
              >
                <HidePasswordIcon></HidePasswordIcon>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  img: {
    flex: 1,
  },
  container: { flex: 1 },
  welcome: {
    color: "white",
  },
  viewContainer: {
    width: 1000,
    height: "100%",
  },
  cards: {
    position: "absolute",
    top: 100,
    left: 100,
  },
  lvlup: {
    position: "absolute",
    top: 250,
    left: 650,
  },
});
