import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import MenuScreen from "./MenuScreen";
import CollectionScreen from "../CollectionScreen/CollectionScreen";
import Friends from "../SettingScreen/Friends/Friends";

export default function SettingNavigator() {
  const SettingNav = createStackNavigator(
    {
      Menu: {
        screen: MenuScreen,
      },
      Collection: {
        screen: CollectionScreen,
      },
      Friends: {
        screen: Friends,
      },
    },

    {
      initialRouteName: "Menu",
      headerMode: "none",
      navigationOptions: {
        headerVisible: false,
      },
    }
  );
  const Container = createAppContainer(SettingNav);

  return <Container />;
}
