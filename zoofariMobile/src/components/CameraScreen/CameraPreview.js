import React from "react";

import {
	ImageBackground,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";

const CameraPreview = ({ img, retake, save, send }) => {
	//TODO ajouter les boutons de reprise de photo et de validation de la photo
	return (
		<View
			style={{
				backgroundColor: "transparent",
				flex: 1,
				width: "100%",
				height: "100%",
			}}
		>
			<ImageBackground
				source={{
					uri: img && img.uri,
				}}
				style={{
					flex: 1,
					height: "83%",
				}}
			>
				<View
					style={{
						flex: 0.83,
						flexDirection: "column",
						padding: 10,
						justifyContent: "flex-end",
					}}
				>
					<View
						style={{
							flexDirection: "row",
							justifyContent: "space-between",
						}}
					>
						<TouchableOpacity onPress={retake} style={styles.touchable}>
							<Text style={styles.text}>Re-take</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={save} style={styles.touchable}>
							<Text style={styles.text}>Save</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={send} style={styles.touchable}>
							<Text style={styles.text}>Send</Text>
						</TouchableOpacity>
					</View>
				</View>
			</ImageBackground>
		</View>
	);
};

const styles = StyleSheet.create({
	touchable: {
		width: 110,
		height: 40,
		alignItems: "center",
		borderRadius: 4,
	},
	text: {
		color: "#fff",
		fontSize: 20,
	},
});
export default CameraPreview;
