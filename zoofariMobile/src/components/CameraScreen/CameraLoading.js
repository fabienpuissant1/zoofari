import { VStack, Text, Spinner } from "native-base";
import React from "react";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function CameraLoading() {
  return (
    <VStack space={4} alignItems="center" style={{ marginTop: "50%" }}>
      <Spinner size="lg" />
      <Text style={[fontStyles.h1, { padding: "10%" }]}>Analyzing</Text>
    </VStack>
  );
}
