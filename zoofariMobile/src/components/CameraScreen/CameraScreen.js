import React, { useRef, useState, useEffect } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Camera } from "expo-camera";
import CameraPreview from "./CameraPreview";
import * as MediaLibrary from "expo-media-library";
import { sendImageToServer } from "../../api/AWS/S3Manager";
import { v4 as uuidv4 } from "uuid";
import "react-native-get-random-values";
import { s3bucketUrl } from "../../api/serverUrl";
import { Api } from "../../api";
import { useSelector, useDispatch } from "react-redux";
import * as Location from "expo-location";
import { useIsFocused } from "@react-navigation/native";

export default function CameraScreen() {
  const [hasPermission, setHasPermission] = useState(null);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [capturedImage, setCapturedImage] = useState(null);
  const [key, setKey] = useState(uuidv4());
  const user = useSelector((state) => state.userState);
  let camera = useRef();
  let isFocused = useIsFocused();
  let dispatch = useDispatch();

  const requestCamera = async () => {
    const { status } = await Camera.requestCameraPermissionsAsync();
    setHasPermission(status === "granted");
  };

  useEffect(() => {
    if (isFocused) {
      requestCamera();
      setKey(uuidv4());
    }
  }, [isFocused]);

  const _takepicture = async () => {
    if (!camera) return;
    const photo = await camera.current.takePictureAsync({
      base64: true,
    });
    setPreviewVisible(true);
    setCapturedImage(photo);
  };

  const retake = () => {
    setPreviewVisible(false);
    setCapturedImage(null);
  };

  const save = async () => {
    const status = await MediaLibrary.requestPermissionsAsync();
    if (!status.granted) {
      return;
    }
    MediaLibrary.createAssetAsync(capturedImage.uri)
      .then(() => {
        alert("The photo has been saved.");
      })
      .catch((err) => {
        alert("An error occured, please try again");
        console.log(err);
      });
  };

  const send = async () => {
    let imageName = uuidv4() + ".jpg";
    let imageUrl = s3bucketUrl + imageName;
    capturedImage.name = imageName;

    if (user.userId !== undefined) {
      dispatch({
        type: "SET_IS_PHOTO_CURRENTLY_UPLOADING",
        value: true,
      });
      try {
        let locate = await Location.getCurrentPositionAsync({
          accuracy: Location.Accuracy.Highest,
        });
        const { latitude, longitude } = locate.coords;
        await sendImageToServer(capturedImage);
        console.log("pushed to bucket");
        await Api.cardService.buyCard({
          userId: user.userId,
          imgUrl: imageUrl,
          latitude: latitude,
          longitude: longitude,
        });
        console.log("pushed to card service");
      } catch (err) {
        store.dispatch({
          type: "SET_IS_PHOTO_CURRENTLY_UPLOADING",
          value: false,
        });
        store.dispatch({
          type: "SET_CARD_MINTED_ERROR",
          value: true,
        });
      }
    }
  };
  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
    <>
      {previewVisible && capturedImage ? (
        <View style={styles.containerflex}>
          <CameraPreview
            img={capturedImage}
            retake={retake}
            save={save}
            send={send}
          />
        </View>
      ) : (
        <View style={styles.containerflex}>
          <Camera
            style={styles.camera}
            ref={camera}
            type={Camera.Constants.Type.back}
            key={key}
          >
            <View style={styles.containerflex}>
              <View style={styles.container}>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity
                    onPress={_takepicture}
                    style={styles.button}
                  />
                </View>
              </View>
            </View>
          </Camera>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  containerflex: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    flexDirection: "row",
  },
  container: {
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    flex: 1,
    width: "100%",
    padding: 20,
    justifyContent: "space-between",
  },
  camera: { width: "100%", height: "83%" },
  buttonContainer: {
    alignSelf: "center",
    flex: 1,
    alignItems: "center",
  },
  button: {
    width: 70,
    height: 70,
    bottom: 0,
    borderRadius: 50,
    backgroundColor: "#fff",
  },
});
