import React, { useEffect, useState, useRef } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import colors from "../../assets/colors/colors";
import {
  checkTokenAndDispatch,
  getUserInfoFromToken,
  refreshUser,
} from "../../api/token";
import SettingNavigator from "../SettingScreen/SettingNavigator";
import SettingsTabBarIcon from "../../assets/icons/IconComponents/SettingsTabBarIcon";
import SettingsTabBarFilledIcon from "../../assets/icons/IconComponents/SettingsTabBarFilledIcon";
import OrdersTabBarIcon from "../../assets/icons/IconComponents/OrdersTabBarIcon";
import OrdersTabBarFilledIcon from "../../assets/icons/IconComponents/OrdersTabBarFilledIcon";
import HomeTabBarIcon from "../../assets/icons/IconComponents/HomeTabBarIcon";
import HomeTabBarFilledIcon from "../../assets/icons/IconComponents/HomeTabBarFilledIcon";
import FavoriteTabBarFilledIcon from "../../assets/icons/IconComponents/FavoriteTabBarFilledIcon";
import FavoriteTabBarIcon from "../../assets/icons/IconComponents/FavoriteTabBarIcon";
import MapScreen from "../MapScreen/MapScreen";
import CameraScreen from "../CameraScreen/CameraScreen";
import MenuNavigator from "../MenuScreen/MenuNavigator";
import CameraLoading from "../CameraScreen/CameraLoading";
import { useDispatch, useSelector } from "react-redux";
import NotificationServer from "../../api/NotificationServer/NotificationServer";
import CardMintedModal from "../Modals/CardMintedModal";
import { useToast, HStack, Spinner, Box, Heading } from "native-base";
import CollectionScreen from "../CollectionScreen/CollectionScreen";
import HeaderBar from "../HeaderBar/HeaderBar";
import Friends from "../SettingScreen/Friends/Friends";
import HandshakeIcon from "../../assets/icons/IconComponents/HandshakeIcon";
import HandshakeFilledIcon from "../../assets/icons/IconComponents/HandshakeFilledIcon";

const Tab = createBottomTabNavigator();

export default function NavBar({ navigation }) {
  const dispatch = useDispatch();
  const [cameraComponent, setCameraComponent] = useState(<CameraScreen />);
  const toast = useToast();
  const toastIsLoading = "toast-loader";
  const toastPhotoUploadingRef = useRef();

  let user = useSelector((state) => state.userState);

  let toastNotification = useSelector(
    (state) => state.mainState.toastNotification
  );

  let isPhotoCurrentlyUploading = useSelector(
    (state) => state.mainState.isPhotoCurrentlyUploading
  );

  let isCardMintedError = useSelector(
    (state) => state.mainState.isCardMintedError
  );

  useEffect(() => {
    if (toastNotification !== null) {
      toast.show({
        title: toastNotification.title,
        status: toastNotification.status,
        placement: "top",
      });

      dispatch({
        type: "SET_TOAST_NOTIFICATION",
        value: null,
      });
    }
  }, [toastNotification]);

  useEffect(() => {
    if (isCardMintedError) {
      toast.show({
        placement: "top-right",
        title: "Card creation error",
        status: "error",
      });
    }
  }, [isCardMintedError]);

  useEffect(() => {
    if (isPhotoCurrentlyUploading) {
      toastPhotoUploadingRef.current = toast.show({
        toastIsLoading,
        render: () => {
          return (
            <Box bg={colors.purple4} px="2" py="1" rounded="sm">
              <HStack space={3} alignItems="center" style={{ padding: 10 }}>
                <Spinner accessibilityLabel="Loading posts" color={"white"} />
                <Heading fontSize="md" color={"white"}>
                  Analyzing photo
                </Heading>
              </HStack>
            </Box>
          );
        },
        duration: null,
        isClosable: false,
        placement: "top-right",
      });
    } else {
      toast.close(toastPhotoUploadingRef.current);
    }
  }, [isPhotoCurrentlyUploading]);

  let isCardMinted = useSelector((state) => state.mainState.isCardMinted);

  let cardMintedInfo = useSelector((state) => state.mainState.cardMintedInfo);

  const handleCloseModalCardMinted = () => {
    dispatch({
      type: "SET_IS_CARD_MINTED",
      value: false,
    });
    dispatch({
      type: "SET_CARD_MINTED_INFO",
      value: {},
    });
  };

  const checkToken = async () => {
    let res = await checkTokenAndDispatch();
    if (!res) {
      navigation.navigate("Login");
    }
    try {
      let userData = await getUserInfoFromToken();
      dispatch({
        type: "SET_USER",
        value: userData,
      });
      if (userData !== undefined) {
        if (userData.userId !== undefined) {
          NotificationServer.connectUser(userData.userId);
          await refreshUser();
        }
      }
    } catch {
      navigation.navigate("Login");
    }
  };

  useEffect(() => {
    checkToken();
  }, []);

  useEffect(() => {
    if (isPhotoCurrentlyUploading) {
      setCameraComponent(<CameraLoading />);
    } else {
      setCameraComponent(<CameraScreen />);
    }
  }, [isPhotoCurrentlyUploading]);

  return (
    <>
      <HeaderBar
        name={user.name + " " + user.surname}
        golds={user.golds !== undefined ? user.golds : ""}
      />
      <NavigationContainer>
        <CardMintedModal
          isOpen={isCardMinted}
          cardInfo={cardMintedInfo}
          handleCloseModal={handleCloseModalCardMinted}
        />
        <Tab.Navigator
          tabBarOptions={{
            keyboardHidesTabBar: true,
            labelStyle: {
              marginBottom: 16,
              marginTop: 4,
            },
            iconStyle: {
              marginTop: 16,
            },
            inactiveTintColor: colors.blue3,
            activeTintColor: "white",
            style: {
              backgroundColor: "#20C1F4",
              borderRadius: 15,
              marginLeft: 16,
              marginRight: 16,
              marginBottom: 24,
              position: "absolute",
              elevation: 5,
              height: 72,
            },
          }}
        >
          <Tab.Screen
            style={{ marginTop: 10 }}
            name="Map"
            component={MapScreen}
            options={{
              tabBarIcon: ({ focused, color, size }) => (
                <>
                  {focused ? (
                    <HomeTabBarFilledIcon width={24} height={24} />
                  ) : (
                    <HomeTabBarIcon width={24} height={24} />
                  )}
                </>
              ),
            }}
          />
          <Tab.Screen
            name="Catch"
            children={() => cameraComponent}
            options={{
              tabBarIcon: ({ focused, color, size }) => (
                <>
                  {focused ? (
                    <FavoriteTabBarFilledIcon width={24} height={24} />
                  ) : (
                    <FavoriteTabBarIcon width={24} height={24} />
                  )}
                </>
              ),
            }}
          />
          <Tab.Screen
            name="Collection"
            component={CollectionScreen}
            options={{
              tabBarIcon: ({ focused, color, size }) => (
                <>
                  {focused ? (
                    <OrdersTabBarFilledIcon width={24} height={24} />
                  ) : (
                    <OrdersTabBarIcon width={24} height={24} />
                  )}
                </>
              ),
            }}
          />
          <Tab.Screen
            name="Friends"
            component={Friends}
            options={{
              tabBarIcon: ({ focused, color, size }) => (
                <>
                  {focused ? (
                    <HandshakeFilledIcon width={24} height={24} />
                  ) : (
                    <HandshakeIcon width={24} height={24} />
                  )}
                </>
              ),
            }}
          />
          <Tab.Screen
            name="Settings"
            children={() => <SettingNavigator mainNavigation={navigation} />}
            options={{
              tabBarIcon: ({ focused, color, size }) => (
                <>
                  {focused ? (
                    <SettingsTabBarFilledIcon width={24} height={24} />
                  ) : (
                    <SettingsTabBarIcon width={24} height={24} />
                  )}
                </>
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
}
