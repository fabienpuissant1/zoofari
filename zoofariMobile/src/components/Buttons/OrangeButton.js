import { Button } from "native-base";
import React from "react";
import colors from "../../assets/colors/colors";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function OrangeButton({
  onClick,
  label,
  isLoading,
  isLoadingText,
}) {
  return (
    <Button
      backgroundColor={colors.orange5}
      borderRadius={8}
      onPress={onClick}
      isLoading={isLoading}
      isDisabled={isLoading}
      isLoadingText={isLoadingText}
      _text={{
        fontSize: 16,
        fontFamily: "Raleway-SemiBold",
      }}
    >
      {label}
    </Button>
  );
}
