import React from "react";
import { View, Text } from "native-base";

import { ImageBackground, Image } from "react-native";

import { StyleSheet } from "react-native";

export default function Card({ data }) {
  return (
    <View overflow="hidden" style={styles.container}>
      <ImageBackground
        source={data.cardOverlay}
        alt="Card"
        resizeMode="contain"
        style={styles.cardImage}
      >
        <View style={styles.levelView}>
          <Text style={styles.imageText}>{data.level}</Text>
        </View>

        <View style={styles.powerView}>
          <Text style={styles.imageText}>{data.power}</Text>
        </View>

        <View style={styles.basePowerView}>
          <Text style={styles.imageTextLittle}>
            {"Base : " + data.basePower}
          </Text>
        </View>

        <View style={styles.artworkView}>
          <Image
            source={{ uri: data.artwork }}
            style={{ width: 150, height: 140 }}
          />
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardImage: {
    height: 300,
    width: "100%",
  },
  levelView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    top: 200,
    left: 0,
    right: 120,
    bottom: 0,
  },
  powerView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    top: 200,
    left: 60,
    right: 0,
    bottom: 0,
  },
  basePowerView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    top: 150,
    left: 100,
    right: 0,
    bottom: 0,
  },
  artworkView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    top: 0,
    left: 0,
    right: 0,
    bottom: 45,
  },
  imageTextLittle: {
    fontSize: 10,
    color: "black",
  },
  imageText: {
    fontSize: 20,
    color: "black",
    fontWeight: "bold",
  },
});
