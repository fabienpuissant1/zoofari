import React, { useState, useEffect } from "react";
import { View, Pressable, Text } from "native-base";
import { Api } from "../../api";
import Card from "./Card";
import ModalLevelUp from "../Modals/ModalLevelUp";
import { FlatList } from "react-native";
import { useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import { isUpgradable } from "../../api/final/cardService";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function CollectionScreen() {
  const [cardList, setCardList] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [cardToLevelUp, setCardToLevelUp] = useState({});
  const user = useSelector((state) => state.userState);
  const [totalPower, setTotalPower] = useState(0);
  const isFocused = useIsFocused();

  const fetchCards = async () => {
    if (user.userId !== undefined) {
      let res = await Api.cardService.getUserCards(user.userId);
      let newTotalPower = 0;
      res.forEach((card) => {
        newTotalPower += card.power;
      });
      setTotalPower(newTotalPower);
      res = res.reverse();
      setCardList(res);
    }
  };

  const refreshCards = () => {
    fetchCards();
  };

  useEffect(() => {
    if (isFocused) {
      fetchCards();
    }
  }, [isFocused]);

  const handleOpenLevelUpModal = (card) => {
    setIsModalOpen(true);
    setCardToLevelUp(card);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <View safeArea style={{ marginBottom: 120, marginTop: 25 }}>
      <ModalLevelUp
        isModalOpen={isModalOpen}
        handleCloseModal={handleCloseModal}
        cardToUp={cardToLevelUp}
        cardList={cardList}
        refreshCards={refreshCards}
      />

      <View style={{ marginLeft: 15, marginBottom: 15 }}>
        <Text style={[fontStyles.h2]}>{"Total power : " + totalPower}</Text>
      </View>

      <FlatList
        data={cardList}
        renderItem={({ item }) => (
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              margin: 5,
            }}
          >
            <Pressable
              onPress={
                isUpgradable(item)
                  ? () => {
                      handleOpenLevelUpModal(item);
                    }
                  : () => {}
              }
            >
              <Card data={item} key={item.id} />
            </Pressable>
          </View>
        )}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
