import React from "react";
import { View } from "native-base";
import Card from "../CollectionScreen/Card";
import { FlatList } from "react-native";
import { useSelector } from "react-redux";
import CollectGoldsButton from "../Buttons/CollectGoldsButton";

export default function CardFoundedList({ cardList, isFinding, handleFind }) {
  let user = useSelector((state) => state.userState);

  return (
    <View safeArea>
      <FlatList
        data={cardList}
        keyboardShouldPersistTaps="always"
        renderItem={({ item }) => (
          <View
            style={{
              flex: 1,
              flexDirection: "column",
            }}
          >
            <Card data={item} key={item.id} />
            {!item.isFound && item.userId !== user.userId && (
              <View
                style={{
                  paddingLeft: "10%",
                  paddingRight: "10%",
                  marginBottom: 15,
                }}
              >
                <CollectGoldsButton
                  label={"collect " + item.basePower + " 💰"}
                  onClick={() => {
                    handleFind(item);
                  }}
                  isLoading={isFinding}
                />
              </View>
            )}
          </View>
        )}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
