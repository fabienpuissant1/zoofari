import React, { useRef, useEffect, useState } from "react";
import { View, SafeAreaView, StyleSheet, StatusBar } from "react-native";
import * as Location from "expo-location";
import { WebView } from "react-native-webview";
import { useIsFocused } from "@react-navigation/native";
import { Api } from "../../api/index";
import ModalFoundedCards from "../Modals/ModalFoundedCards";
import LeafletHtml from "./LeafletHtml";
import { groubByDistance } from "../../api/MapApi/utils";
import { formatCardList } from "../../api/final/cardService";

function MapScreen() {
  let mapRef = useRef();
  const [lat, setLat] = useState(0);
  const [long, setLong] = useState(0);
  const [timer, setTimer] = useState(null);
  const [cardGroups, setCardGroups] = useState([]);
  const [cardListModal, setCardListModal] = useState([]);
  const [isCardFoundedOpen, setIsCardFoundedOpen] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    goToMyPosition(lat, long);
    if (lat !== 0 && long !== 0) {
      checkCardsArround();
    }
  }, [lat, long]);

  const findModalCard = (card) => {
    let newModalCards = [...cardListModal];
    let cardFinded = newModalCards.find((el) => {
      return el.id === card.id;
    });
    if (cardFinded !== undefined) {
      cardFinded.isFound = true;
    }
    setCardListModal(newModalCards);
  };

  const getLocationAsync = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      setErrorMessage("Permission to access location was denied");
    }

    let locate = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.Highest,
    });
    const { latitude, longitude } = locate.coords;
    setLat(latitude);
    setLong(longitude);
  };

  const checkCardsArround = async () => {
    let cardsArround = await Api.cardService.getCardsArround(lat, long);
    let formattedCardList = formatCardList(cardsArround);
    let groupedCard = groubByDistance(formattedCardList);
    setCardGroups(groupedCard);
    let string = "";
    groupedCard.forEach((cardGroup, index) => {
      if (cardGroup[0] !== undefined) {
        string += `
        let circle${index} = L.circle([${cardGroup[0].latitude}, ${cardGroup[0].longitude}], {color: 'red', radius: 10});
        circle${index}.on("click", function(){
          window.ReactNativeWebView.postMessage(${index})
        })
        creatures.addLayer(circle${index});
        creatures.addTo(map)
      `;
      }
    });
    mapRef.current.injectJavaScript(`creatures.clearLayers()` + string);
  };

  const handleClickMap = (e) => {
    let groupIndex = e.nativeEvent.data;
    if (cardGroups[groupIndex] !== undefined) {
      setCardListModal(cardGroups[groupIndex]);
      setIsCardFoundedOpen(true);
    }
  };

  const handleCloseModal = () => {
    setIsCardFoundedOpen(false);
  };

  useEffect(() => {
    if (isFocused) {
      getLocationAsync();
      const timer = setInterval(() => {
        getLocationAsync();
      }, 15000);
      setTimer(timer);
    } else {
      clearInterval(timer);
    }
  }, [isFocused]);

  const goToMyPosition = (lat, lon) => {
    mapRef.current.injectJavaScript(`
        map.setView([${lat}, ${lon}]);
        mainMarker.setLatLng([${lat}, ${lon}]);
      `);
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ModalFoundedCards
          isModalOpen={isCardFoundedOpen}
          handleCloseModal={handleCloseModal}
          cardList={cardListModal}
          findModalCard={findModalCard}
        />
        <View style={styles.Webview}>
          <WebView
            ref={mapRef}
            source={{ html: LeafletHtml }}
            style={styles.Webview}
            onMessage={handleClickMap}
          />
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
    backgroundColor: "grey",
  },
  Webview: {
    height: "100%",
  },
  ButtonArea: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  Button: {
    width: 80,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "black",
    alignItems: "center",
  },
  ButtonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 14,
  },
});

export default MapScreen;
