import React from "react";
import { Center, AlertDialog, Button, Text } from "native-base";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function ConfirmPopup({
  isOpen,
  handleCancel,
  handleConfirm,
  confirmLabel,
  headerText,
  bodyText,
  cancelLabel = "Cancel",
}) {
  return (
    <Center>
      <AlertDialog isOpen={isOpen} onClose={handleCancel}>
        <AlertDialog.Content>
          <AlertDialog.CloseButton />
          <AlertDialog.Header>
            <Text style={fontStyles.h2}>{headerText}</Text>
          </AlertDialog.Header>
          <AlertDialog.Body>
            <Text style={fontStyles.p3}>{bodyText}</Text>
          </AlertDialog.Body>
          <AlertDialog.Footer>
            <Button.Group space={2}>
              <Button
                variant="unstyled"
                colorScheme="coolGray"
                onPress={handleCancel}
                _text={{
                  fontFamily: "Raleway-SemiBold",
                }}
              >
                {cancelLabel}
              </Button>
              <Button
                onPress={handleConfirm}
                _text={{
                  fontFamily: "Raleway-SemiBold",
                }}
              >
                {confirmLabel}
              </Button>
            </Button.Group>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>
    </Center>
  );
}
