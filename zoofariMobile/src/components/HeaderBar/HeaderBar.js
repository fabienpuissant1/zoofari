import React from "react";
import { View, HStack, Spacer, Text } from "native-base";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import fontStyle from "../../assets/fontStyles/fontStyles";

export default function HeaderBar({ name, golds }) {
  return (
    <View style={styles.headerBarContainer}>
      <HStack>
        <View style={styles.leftContainer}>
          <Text style={[styles.text, fontStyle.h2]}>{name}</Text>
        </View>
        <Spacer />
        <View style={styles.rightContainer}>
          <Text style={[styles.text, fontStyle.h2]}>{golds + " 💰"}</Text>
        </View>
      </HStack>
    </View>
  );
}
const styles = StyleSheet.create({
  headerBarContainer: {
    height: 50,
    backgroundColor: colors.purple2,
  },
  leftContainer: {
    marginLeft: 30,
  },
  text: {
    marginTop: 15,
    color: colors.purple5,
  },
  rightContainer: {
    marginRight: 30,
  },
});
