import React from "react";
import { Flex, Text, Spacer, Pressable } from "native-base";
import { StyleSheet } from "react-native";
import fontStyles from "../../../assets/fontStyles/fontStyles";
import BlueArrowRightIcon from "../../../assets/icons/IconComponents/BlueArrowRightIcon";

export default function AccountInfoButton({ label, onClick }) {
  return (
    <Pressable onPress={onClick}>
      <Flex direction="row" style={styles.flexInfo}>
        <Text style={fontStyles.p2}>{label}</Text>
        <Spacer />
        <Text style={styles.iconContainer}>
          <BlueArrowRightIcon width={16} height={16} />
        </Text>
      </Flex>
    </Pressable>
  );
}
const styles = StyleSheet.create({
  flexInfo: {
    marginBottom: 24,
  },
  iconContainer: {
    alignSelf: "center",
  },
});
