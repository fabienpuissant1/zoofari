import React from "react";
import { View, Text } from "native-base";
import { StyleSheet } from "react-native";
import colors from "../../../assets/colors/colors";
import fontStyles from "../../../assets/fontStyles/fontStyles";

export default function BackButton({ handleGoBack }) {
  return (
    <View>
      <Text style={[styles.backButton, fontStyles.h3]} onPress={handleGoBack}>
        Back
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  backButton: {
    color: colors.purple5,
    marginTop: 24,
    marginBottom: 33,
  },
});
