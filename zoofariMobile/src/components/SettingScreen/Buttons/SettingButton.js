import { Button } from "native-base";
import React from "react";
import colors from "../../../assets/colors/colors";
import { StyleSheet } from "react-native";
import BlueArrowRightIcon from "../../../assets/icons/IconComponents/BlueArrowRightIcon";

export default function SettingButton({ label, endIcon, onclick }) {
  return (
    <Button
      _text={{
        color: colors.grey8,
        fontFamily: "Raleway-SemiBold",
        fontSize: 16,
      }}
      _stack={{
        display: "flex",
        marginLeft: 0,
        justifyContent: "space-between",
        flexGrow: 3,
      }}
      style={styles.shadowProp}
      borderRadius={10}
      backgroundColor="white"
      onPress={onclick}
      endIcon={endIcon && <BlueArrowRightIcon width={16} height={16} />}
    >
      {label}
    </Button>
  );
}
const styles = StyleSheet.create({
  endIcon: {
    marginLeft: "10%",
  },
  shadowProp: {
    shadowColor: "#171717",
    shadowOffset: { width: -1, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
});
