import { View, ScrollView, Box, Text } from "native-base";
import React from "react";
import OrangeButton from "../Buttons/OrangeButton";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import SettingButton from "./Buttons/SettingButton";
import fontStyles from "../../assets/fontStyles/fontStyles";
import NotificationServer from "../../api/NotificationServer/NotificationServer";

export default function SettingScreen({ navigation }) {
  return (
    <ScrollView>
      <Box safeArea>
        <View>
          <Text style={[styles.settingTitle, fontStyles.h3]}>Settings</Text>
        </View>

        <View style={styles.infoButton}>
          <SettingButton
            label="Account information"
            onclick={() => {
              navigation.navigate("AccountInfo");
            }}
            endIcon
          />
        </View>

        <View style={{ marginLeft: 20, marginRight: 20, marginTop: 20 }}>
          <OrangeButton
            label="Log out"
            onClick={() => {
              NotificationServer.disconnectUser();
              navigation.getParam("mainNavigation").navigate("Login");
            }}
          />
        </View>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  settingTitle: {
    color: colors.grey8,
    marginLeft: 24,
    marginTop: 24,
    marginBottom: 40,
  },
  infoButton: {
    marginTop: 8,
    marginLeft: 24,
    marginRight: 24,
  },
});
