import { Box, Flex, Spacer, View } from "native-base";
import React from "react";
import { ScrollView, Text } from "react-native";
import BackButton from "../Buttons/BackButton";
import { StyleSheet } from "react-native";
import colors from "../../../assets/colors/colors";
import AccountInfoButton from "../Buttons/AccountInfoButton";
import SubScreenTitle from "../SubScreenTitle";
import fontStyles from "../../../assets/fontStyles/fontStyles";

export default function AccountInformation({ navigation }) {
  const handleGoBack = () => {
    navigation.getParam("settingNavigation").navigate("Main");
  };

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <BackButton handleGoBack={handleGoBack} />

        <SubScreenTitle title="Account information" />

        <View>
          <AccountInfoButton
            label="Change email"
            onClick={() => {
              navigation.navigate("ChangeEmail");
            }}
          />
        </View>

        <View>
          <AccountInfoButton
            label="Change password"
            onClick={() => {
              navigation.navigate("ChangePassword");
            }}
          />
        </View>

        <View>
          <AccountInfoButton
            label="Change personal account information"
            onClick={() => {
              navigation.navigate("ChangeAccountInfo");
            }}
          />
        </View>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  mainContainer: {
    marginLeft: 24,
    marginRight: 24,
  },
  deleteAccountText: {
    marginTop: 14,
    color: colors.red6,
  },
});
