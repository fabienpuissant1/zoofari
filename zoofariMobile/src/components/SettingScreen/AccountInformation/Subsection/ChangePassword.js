import { Box, ScrollView, View, Text, useToast } from "native-base";
import React, { useState, useEffect } from "react";
import BackButton from "../../Buttons/BackButton";
import SubScreenTitle from "../../SubScreenTitle";
import SubsectionTitle from "./SubsectionTitle";
import { StyleSheet } from "react-native";
import PasswordInput from "../../../Inputs/PasswordInput";
import fontStyles from "../../../../assets/fontStyles/fontStyles";
import colors from "../../../../assets/colors/colors";
import OrangeButton from "../../../Buttons/OrangeButton";
import { isNOE } from "../../../../utils";
import { Api } from "../../../../api";

export default function ChangePassword({ navigation }) {
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmNewPassword, setConfirmNewPassword] = useState("");
  const [isNewPasswordInvalid, setIsNewPasswordInvalid] = useState(false);
  const [isConfirmNewPasswordInvalid, setIsConfirmNewPasswordInvalid] =
    useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();

  const handleGoBack = () => {
    navigation.navigate("Main");
  };

  const handleChangePassword = () => {
    if (!isNOE(newPassword) && !isNOE(password) && !isNOE(confirmNewPassword)) {
      if (!isNewPasswordInvalid && !isConfirmNewPasswordInvalid) {
        setIsLoading(true);
        let data = {
          oldPassword: password,
          password: newPassword,
        };
        Api.authService
          .updatePassword(data)
          .then(() => {
            toast.show({
              title: "The password has been successfully modified",
              status: "success",
              placement: "top",
            });
            setPassword("");
            setNewPassword("");
            setConfirmNewPassword("");
          })
          .catch((err) => {
            toast.show({
              title: "Old password incorrect",
              status: "error",
              placement: "top",
            });
          })
          .finally(() => {
            setIsLoading(false);
          });
      }
    }
  };

  const handlePasswordChanged = (value) => {
    setPassword(value);
  };

  const handleNewPasswordChanged = (value) => {
    setNewPassword(value);
  };

  const handleConfirmNewPasswordChanged = (value) => {
    setConfirmNewPassword(value);
  };

  useEffect(() => {
    if (newPassword === "") {
      setIsNewPasswordInvalid(false);
    } else if (newPassword.length > 7) {
      setIsNewPasswordInvalid(false);
    } else {
      setIsNewPasswordInvalid(true);
    }
  }, [newPassword]);

  useEffect(() => {
    if (confirmNewPassword === "") {
      setIsConfirmNewPasswordInvalid(false);
    } else if (newPassword === confirmNewPassword) {
      setIsConfirmNewPasswordInvalid(false);
    } else {
      setIsConfirmNewPasswordInvalid(true);
    }
  }, [confirmNewPassword, newPassword]);

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <BackButton handleGoBack={handleGoBack} />
        <SubScreenTitle title="Account information" />
        <SubsectionTitle
          title="Change password"
          description="At least 8 characters needed"
        />

        <View style={styles.margin5}>
          <PasswordInput
            placeholder="Password"
            title="Password"
            value={password}
            onValueChanged={handlePasswordChanged}
          />
        </View>

        <View style={styles.margin5}>
          <PasswordInput
            placeholder="New password"
            title="New password"
            value={newPassword}
            isInvalid={isNewPasswordInvalid}
            onValueChanged={handleNewPasswordChanged}
          />
        </View>

        <View>
          <Text
            style={[
              styles.rulePasswordText,
              { color: isNewPasswordInvalid ? colors.red5 : colors.grey5 },
              fontStyles.p4,
            ]}
          >
            At least 8 characters needed
          </Text>
        </View>

        <View>
          <PasswordInput
            placeholder="Password"
            title="Confrim password"
            value={confirmNewPassword}
            isInvalid={isConfirmNewPasswordInvalid}
            onValueChanged={handleConfirmNewPasswordChanged}
          />
        </View>

        {isConfirmNewPasswordInvalid && (
          <View>
            <Text
              style={[
                styles.rulePasswordText,
                { color: colors.red5 },
                fontStyles.p4,
              ]}
            >
              Passwords don't match
            </Text>
          </View>
        )}

        <View style={styles.margin5}>
          <OrangeButton
            label="Save"
            onClick={handleChangePassword}
            isLoading={isLoading}
          />
        </View>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  mainContainer: {
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 120,
  },
  margin5: {
    marginTop: "5%",
  },
  margin1: {
    marginTop: "1%",
  },
  rulePasswordText: {
    marginTop: 8,
    marginBottom: 16,
    marginLeft: 4,
  },
});
