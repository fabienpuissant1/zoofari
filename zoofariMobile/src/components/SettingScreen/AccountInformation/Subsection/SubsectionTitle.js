import { View, Text } from "native-base";
import React from "react";
import { StyleSheet } from "react-native";
import colors from "../../../../assets/colors/colors";
import fontStyles from "../../../../assets/fontStyles/fontStyles";

export default function SubsectionTitle({ title, description }) {
  return (
    <>
      <View style={styles.sectionTitleContainer}>
        <Text style={[fontStyles.h2, styles.sectionTitle]}>{title}</Text>
      </View>

      {description && (
        <View style={styles.sectionDescriptionContainer}>
          <Text style={[fontStyles.p3, styles.sectionDescription]}>
            {description}
          </Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  sectionTitleContainer: {
    marginTop: "1%",
  },
  sectionTitle: {
    color: colors.grey8,
    alignContent: "center",
  },
  sectionDescriptionContainer: {
    marginTop: "2%",
  },
  sectionDescription: {
    color: colors.grey6,
  },
});
