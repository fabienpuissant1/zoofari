import { Box, ScrollView, View, useToast } from "native-base";
import React, { useState } from "react";
import BackButton from "../../Buttons/BackButton";
import SubScreenTitle from "../../SubScreenTitle";
import SubsectionTitle from "./SubsectionTitle";
import { StyleSheet } from "react-native";
import TextInput from "../../../Inputs/TextInput";
import OrangeButton from "../../../Buttons/OrangeButton";
import { isNOE } from "../../../../utils";
import { Api } from "../../../../api";
import { useDispatch, useSelector } from "react-redux";

export default function ChangeAccountInfo({ navigation }) {
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();
  let user = useSelector((state) => state.userState);
  const dispatch = useDispatch();

  const [firstName, setFirstName] = useState(user.name);
  const [surName, setSurName] = useState(user.surname);

  const handleChangeFirstName = (value) => {
    setFirstName(value);
  };

  const handleChangeSurName = (value) => {
    setSurName(value);
  };

  const handleGoBack = () => {
    navigation.navigate("Main");
  };

  const handleSave = async () => {
    if (!isNOE(firstName) && !isNOE(surName)) {
      setIsLoading(true);

      let newUser = {
        userId: user.userId,
        name: firstName,
        surname: surName,
      };
      try {
        let res = await Api.userService.updatePersonalInfo(newUser);
        dispatch({
          type: "UPDATE_PERSONAL_INFO",
          value: {
            name: res.name,
            surname: res.surname,
          },
        });
        toast.show({
          title: "Information saved",
          status: "success",
          placement: "top",
        });
      } catch {
        toast.show({
          title: "Server error",
          status: "error",
          placement: "top",
        });
      }
    }
  };

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <BackButton handleGoBack={handleGoBack} />
        <SubScreenTitle title="Account information" />
        <SubsectionTitle title="Personal information" />

        <View style={styles.margin}>
          <TextInput
            title="Firstname"
            value={firstName}
            onValueChanged={handleChangeFirstName}
            placeholder="Firstname"
          />
        </View>

        <View style={styles.margin}>
          <TextInput
            title="Lastname"
            value={surName}
            onValueChanged={handleChangeSurName}
            placeholder="Lastname"
          />
        </View>

        <View style={styles.margin}>
          <OrangeButton
            label="Save"
            onClick={handleSave}
            isLoading={isLoading}
          />
        </View>
      </Box>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    marginLeft: 24,
    marginRight: 24,
  },
  margin: {
    marginTop: "5%",
  },
});
