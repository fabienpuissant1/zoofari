import { Box, ScrollView, View, useToast } from "native-base";
import React, { useState } from "react";
import BackButton from "../../Buttons/BackButton";
import { StyleSheet } from "react-native";
import SubScreenTitle from "../../SubScreenTitle";
import SubsectionTitle from "./SubsectionTitle";
import EmailInput from "../../../Inputs/EmailInput";
import OrangeButton from "../../../Buttons/OrangeButton";
import { Api } from "../../../../api";

export default function ChangeEmail({ navigation }) {
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();

  const handleGoBack = () => {
    navigation.navigate("Main");
  };

  const handleChangeEmail = (value) => {
    setEmail(value);
  };

  const handleSaveEmail = () => {
    if (email !== "") {
      setIsLoading(true);
      Api.authService
        .updateEmail(email)
        .then(() => {
          toast.show({
            title: "The email has been successfully modified",
            status: "success",
            placement: "top",
          });
          navigation
            .getParam("settingNavigation")
            .getParam("mainNavigation")
            .navigate("Login");
        })
        .catch(() => {
          toast.show({
            title: "Server error",
            status: "error",
            placement: "top",
          });
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  return (
    <ScrollView>
      <Box safeArea style={styles.mainContainer}>
        <BackButton handleGoBack={handleGoBack} />
        <SubScreenTitle title="Account information" />
        <SubsectionTitle title="Change email" />

        <View style={styles.margin5}>
          <EmailInput
            title="New email address"
            placeholder="email@email.com"
            value={email}
            onValueChanged={handleChangeEmail}
          />
        </View>

        <View style={styles.margin5}>
          <OrangeButton
            label="Save"
            onClick={handleSaveEmail}
            isLoading={isLoading}
          />
        </View>
      </Box>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  mainContainer: {
    marginLeft: 24,
    marginRight: 24,
  },
  margin5: {
    marginTop: "10%",
  },
});
