import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import AccountInformation from "./AccountInformation";
import ChangeEmail from "./Subsection/ChangeEmail";
import ChangePassword from "./Subsection/ChangePassword";
import ChangeAccountInfo from "./Subsection/ChangeAccountInfo";

export default function AccountInformationNavigator({ navigation }) {
  const AccountInfoNav = createStackNavigator(
    {
      Main: {
        screen: AccountInformation,
        params: {
          settingNavigation: navigation,
        },
      },
      ChangeEmail: {
        screen: ChangeEmail,
        params: {
          settingNavigation: navigation,
        },
      },
      ChangePassword: ChangePassword,
      ChangeAccountInfo: ChangeAccountInfo,
    },

    {
      initialRouteName: "Main",
      headerMode: "none",
      navigationOptions: {
        headerVisible: false,
      },
    }
  );
  const Container = createAppContainer(AccountInfoNav);

  return <Container settingNavigation={navigation} />;
}
