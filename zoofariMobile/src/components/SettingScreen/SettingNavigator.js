import React from "react";
import SettingScreen from "./SettingScreen";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import AccountInformationNavigator from "./AccountInformation/AccountInformationNavigator";
import Friends from "./Friends/Friends";

export default function SettingNavigator({ mainNavigation }) {
  const SettingNav = createStackNavigator(
    {
      Main: {
        screen: SettingScreen,
        params: { mainNavigation: mainNavigation },
      },
      AccountInfo: {
        screen: AccountInformationNavigator,
        params: { mainNavigation: mainNavigation },
      },
    },

    {
      initialRouteName: "Main",
      headerMode: "none",
      navigationOptions: {
        headerVisible: false,
      },
    }
  );
  const Container = createAppContainer(SettingNav);

  return <Container mainNavigation={mainNavigation} />;
}
