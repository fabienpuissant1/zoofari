import React, { useEffect, useState } from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import {
  View,
  Button,
  Modal,
  HStack,
  Spacer,
  useToast,
  VStack,
  Badge,
} from "native-base";
import colors from "../../../assets/colors/colors";
import fontStyles from "../../../assets/fontStyles/fontStyles";
import FriendElement from "./UserElement";
import OrangeButton from "../../Buttons/OrangeButton";
import { useDispatch, useSelector } from "react-redux";
import SearchBarInput from "../../Inputs/SearchBarInput";
import BackButton from "../Buttons/BackButton";
import SubScreenTitle from "../SubScreenTitle";
import { Api } from "../../../api";
import UserList from "./UserList";
import PendingInvitList from "./PendingInvitList";
import { useIsFocused } from "@react-navigation/native";
import ConfirmPopup from "../../Popup/ConfirmPopup";

export default function Friends({ navigation }) {
  let isFocused = useIsFocused();
  let dispatch = useDispatch();
  let toast = useToast();

  let user = useSelector((state) => state.userState);

  let connectedUsers = useSelector((state) => state.mainState.connectedUsers);

  const [friendList, setFriendList] = useState([]);
  const [invitList, setInvitList] = useState([]);

  const [modalShow, setModalShow] = useState(false);
  const [invitModalShow, setInvitModalShow] = useState(false);

  const [search, setSearch] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  const [userList, setUserList] = useState([]);
  const [isSelectedList, setIsSelectedList] = useState([]);

  const [isConfirmDeleteModalOpen, setIsConfirmDeleteModalOpen] =
    useState(false);

  const [userToDelete, setUserToDelete] = useState({});

  const handleChangeSearch = (e) => {
    setSearch(e);
  };

  const handleSendFriendRequests = async () => {
    let allPass = true;
    for (let i = 0; i < isSelectedList.length; i++) {
      let res = await Api.userService.sendInvite(
        user.userId,
        isSelectedList[i]
      );
      if (!res) {
        allPass = false;
      }
    }
    if (allPass) {
      notifyToast("Friend request sent", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Server issue", "error");
    }
    setModalShow(false);
    setIsSelectedList([]);
    refreshStoreFriends();
    refreshStoreInvites();
  };

  const fetchUsers = async () => {
    let userList = await Api.userService.getAllUsers();
    setUserList(userList);
    setSearchResult(userList.filter(filtre));
  };

  const onClick = (user) => {
    let isSelected = [...isSelectedList];
    if (isSelected.includes(user.userId)) {
      let index = isSelected.indexOf(user.userId);
      if (index !== -1) {
        isSelected.splice(index, 1);
      }
    } else {
      isSelected.push(user.userId);
    }
    setIsSelectedList(isSelected);
  };

  const onAcceptAll = async (idList) => {
    let allPass = true;
    for (let i = 0; i < idList.length; i++) {
      let res = await Api.userService.acceptFriend(user.userId, idList[i]);
      if (!res) {
        allPass = false;
      }
    }
    if (allPass) {
      notifyToast("Successfully rejected", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Unsuccessfully rejected", "error");
    }
    refreshStoreFriends();
    refreshStoreInvites();
  };

  const onAccept = async (friendId) => {
    let res = await Api.userService.acceptFriend(user.userId, friendId);
    if (res) {
      notifyToast("Successfully accepted", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Unsuccessfully accepted", "error");
    }
    refreshStoreFriends();
    refreshStoreInvites();
  };

  const onRefuse = async (friendId) => {
    let res = await Api.userService.declineFriend(user.userId, friendId);
    if (res) {
      notifyToast("Successfully rejected", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Unsuccessfully rejected", "error");
    }
    refreshStoreFriends();
    refreshStoreInvites();
  };

  const onRefuseAll = async (idList) => {
    let allPass = true;
    for (let i = 0; i < idList.length; i++) {
      let res = await Api.userService.declineFriend(user.userId, idList[i]);
      if (!res) {
        allPass = false;
      }
    }
    if (allPass) {
      notifyToast("Successfully rejected", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Unsuccessfully rejected", "error");
    }
    refreshStoreFriends();
    refreshStoreInvites();
  };

  const onTryToDeleteFriends = (userToDelete) => {
    setUserToDelete(userToDelete);
    setIsConfirmDeleteModalOpen(true);
  };

  const handleConfirmDelete = async () => {
    let res = await Api.userService.removeFriend(
      user.userId,
      userToDelete.userId
    );
    if (res) {
      notifyToast("Successfully deleted", "success");
      refreshStoreFriends();
    } else {
      notifyToast("Unsuccessfully deleted", "error");
    }
    setIsConfirmDeleteModalOpen(false);
  };

  const filtre = (el) => {
    if (el.userId === user.userId) {
      return false;
    }
    return (
      (el.name.includes(search) || el.surname.includes(search)) &&
      !user.friendList.includes(el.userId) &&
      !user.pendingList.includes(el.userId)
    );
  };

  const refreshStoreFriends = async () => {
    let userFriendList = await Api.userService.getFriendList(user.userId);
    setFriendList(userFriendList);
    setIsSelectedList([]);
  };

  const refreshStoreInvites = async () => {
    let pendingInvite = await Api.userService.getPendingInvite(user.userId);
    setInvitList(pendingInvite);
  };

  useEffect(() => {
    if (user.userId !== undefined) {
      fetchUsers();
    }
  }, []);

  useEffect(() => {
    if (user.userId !== undefined) {
      let newUserList = userList.filter(filtre);
      setSearchResult(newUserList);
    }
  }, [search, user, userList]);

  useEffect(() => {
    if (user.userId !== undefined) {
      refreshStoreFriends();
      refreshStoreInvites();
    }
  }, [isFocused, user]);

  const notifyToast = (title, status) => {
    toast.show({
      title,
      status,
      placement: "top",
    });
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ConfirmPopup
          isOpen={isConfirmDeleteModalOpen}
          handleCancel={() => setIsConfirmDeleteModalOpen(false)}
          handleConfirm={handleConfirmDelete}
          confirmLabel="Delete"
          headerText="Confirm"
          bodyText="Are you sure you want to delete"
        />

        <SubScreenTitle title="Friends" />
        <View style={styles.footer}>
          <HStack space={2} alignItems="center">
            <OrangeButton
              label="Add Friends"
              onClick={() => {
                setModalShow(true);
              }}
            />
            <Spacer />
            <VStack>
              <Badge
                colorScheme="info"
                rounded="999px"
                mb={-4}
                mr={-4}
                zIndex={1}
                variant="solid"
                alignSelf="flex-end"
                _text={{
                  fontSize: 12,
                }}
              >
                {invitList.length}
              </Badge>
              <OrangeButton
                label="Invitations"
                onClick={() => {
                  setInvitModalShow(true);
                }}
                style={styles.button}
              />
            </VStack>
          </HStack>
        </View>

        <UserList
          onTryToDeleteFriends={onTryToDeleteFriends}
          connectedUsers={connectedUsers}
          userList={friendList}
          isSelectedList={isSelectedList}
          isFriendList
        ></UserList>

        <Modal
          isOpen={modalShow}
          onClose={() => {
            setModalShow(false);
          }}
        >
          <Modal.Content maxWidth="400px">
            <Modal.CloseButton />
            <Modal.Header>Add friend</Modal.Header>
            <Modal.Body>
              <SearchBarInput
                value={search}
                onValueChanged={handleChangeSearch}
              ></SearchBarInput>
              <UserList
                userList={searchResult}
                isClickable
                isSelectedList={isSelectedList}
                onClick={(e) => {
                  onClick(e);
                }}
              ></UserList>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group space={2}>
                <Button
                  variant="ghost"
                  colorScheme="blueGray"
                  onPress={() => {
                    setModalShow(false);
                  }}
                >
                  Cancel
                </Button>
                <Button
                  onPress={() => {
                    handleSendFriendRequests();
                  }}
                >
                  Add Selected
                </Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>
        <Modal
          isOpen={invitModalShow}
          onClose={() => {
            setInvitModalShow(false);
          }}
        >
          <Modal.Content maxWidth="400px">
            <Modal.CloseButton />
            <Modal.Header>Friend requests</Modal.Header>
            <Modal.Body>
              <PendingInvitList
                userList={invitList}
                onAccept={onAccept}
                onRefuse={onRefuse}
              ></PendingInvitList>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group space={2}>
                <Button
                  variant="ghost"
                  colorScheme="blueGray"
                  onPress={() => {
                    onRefuseAll(invitList);
                    setModalShow(false);
                  }}
                >
                  Decline all
                </Button>
                <Button
                  onPress={() => {
                    onAcceptAll(invitList);
                    setModalShow(false);
                  }}
                >
                  Accept all
                </Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  settingTitle: {
    color: colors.grey8,
    marginTop: 40,
    marginBottom: 40,
    alignSelf: "center",
  },
  footer: {
    marginBottom: 15,
  },
  button: {
    marginLeft: 30,
  },
  container: {
    margin: 24,
    height: "83%",
  },
});
