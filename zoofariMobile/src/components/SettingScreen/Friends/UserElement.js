import { View, Text, Center, HStack, Spacer, Pressable } from "native-base";
import React from "react";
import fontStyles from "../../../assets/fontStyles/fontStyles";
import colors from "../../../assets/colors/colors";
import OrangeCloseIcon from "../../../assets/icons/IconComponents/OrangeCloseIcon";
import { CircleIcon } from "native-base";

export default function UserElement({
  user,
  isSelectedList,
  onTryToDeleteFriends,
  isFriendList,
  connectedUsers = [],
}) {
  return (
    <Center
      style={[
        {
          height: 50,
          margin: 5,
          elevation: 3,
          borderRadius: 15,
          padding: 15,
          backgroundColor:
            isSelectedList !== undefined && isSelectedList.includes(user.userId)
              ? colors.blue3
              : "white",
        },
        fontStyles.p2,
      ]}
    >
      <HStack space={2} alignItems="center" style={{ margin: 10 }}>
        {connectedUsers.includes(user.userId) && (
          <CircleIcon color={colors.green6} size={"4"} />
        )}
        <Text>{user.name + " " + user.surname}</Text>
        <Spacer />
        {isFriendList && (
          <Pressable onPress={() => onTryToDeleteFriends(user)}>
            <OrangeCloseIcon width="24px" height="24px" />
          </Pressable>
        )}
      </HStack>
    </Center>
  );
}
