import React from "react";
import { ScrollView, Box, Center, Text, HStack, Spacer } from "native-base";
import { Pressable } from "react-native";
import OrangeCloseIcon from "../../../assets/icons/IconComponents/OrangeCloseIcon";
import BluePlusIcon from "../../../assets/icons/IconComponents/BluePlusIcon";
import fontStyles from "../../../assets/fontStyles/fontStyles";

export default function PendingInvitList({ userList, onAccept, onRefuse }) {
  return (
    <ScrollView>
      <Box safeArea>
        {userList.map((user) => {
          return (
            <Center
              style={[
                {
                  height: 50,
                  margin: 5,
                  borderRadius: 15,
                  padding: 15,
                  elevation: 3,
                  backgroundColor: "white",
                },
                fontStyles.p2,
              ]}
              key={user.userId}
            >
              <HStack space={2} alignItems="center" style={{ margin: 10 }}>
                <Text>{user.name + " " + user.surname}</Text>
                <Spacer />
                <Pressable
                  onPress={() => {
                    onAccept(user.userId);
                  }}
                >
                  <BluePlusIcon width="24px" height="24px" />
                </Pressable>
                <Pressable
                  onPress={() => {
                    onRefuse(user.userId);
                  }}
                >
                  <OrangeCloseIcon width="24px" height="24px" />
                </Pressable>
              </HStack>
            </Center>
          );
        })}
      </Box>
    </ScrollView>
  );
}
