import React from "react";
import UserElement from "./UserElement";
import { ScrollView, Box } from "native-base";
import { Pressable } from "react-native";

export default function UserList({
  userList,
  onClick,
  isSelectedList,
  isClickable,
  onTryToDeleteFriends,
  isFriendList,
  connectedUsers,
}) {
  return (
    <ScrollView>
      <Box safeArea>
        {userList.map((u) => {
          return (
            <Pressable
              onPress={
                isClickable
                  ? () => {
                      onClick(u);
                    }
                  : () => {}
              }
              key={u.userId}
            >
              <UserElement
                user={u}
                connectedUsers={connectedUsers}
                isSelectedList={isSelectedList}
                onTryToDeleteFriends={onTryToDeleteFriends}
                isFriendList={isFriendList}
              ></UserElement>
            </Pressable>
          );
        })}
      </Box>
    </ScrollView>
  );
}
