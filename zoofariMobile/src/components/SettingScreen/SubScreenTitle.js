import React from "react";
import { View, Text } from "react-native";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function SubScreenTitle({ title }) {
  return (
    <View style={styles.paramText}>
      <Text style={fontStyles.h3}>{title}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  paramText: {
    color: colors.grey8,
    marginBottom: 20,
  },
});
