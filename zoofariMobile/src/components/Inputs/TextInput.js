import { Input, View, Text } from "native-base";
import React from "react";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function TextInput({
  value,
  onValueChanged,
  placeholder,
  title,
}) {
  return (
    <View>
      <Text style={[styles.titleText, fontStyles.p2]}>{title}</Text>
      <Input
        variant="underlined"
        type="text"
        placeholder={placeholder}
        placeholderTextColor={colors.grey3}
        value={value}
        onChangeText={onValueChanged}
        borderRadius={15}
        backgroundColor="white"
        borderTopLeftRadius={8}
        borderTopRightRadius={8}
        borderBottomLeftRadius={1}
        borderBottomRightRadius={1}
        borderBottomColor={colors.blue3}
        borderBottomWidth={2}
        fontFamily="Raleway-Regular"
      />
    </View>
  );
}
const styles = StyleSheet.create({
  titleText: {
    color: colors.grey6,
    marginLeft: 4,
    marginBottom: 4,
  },
});
