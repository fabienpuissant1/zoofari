import {
  Input,
  View,
  Text,
  Flex,
  Spacer,
  FormControl,
  Pressable,
} from "native-base";
import React, { useState } from "react";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import fontStyles from "../../assets/fontStyles/fontStyles";
import HidePasswordIcon from "../../assets/icons/IconComponents/HidePasswordIcon";
import ShowPasswordIcon from "../../assets/icons/IconComponents/ShowPasswordIcon";

export default function PasswordInput({
  value,
  onValueChanged,
  placeholder,
  title,
  forgotPassword,
  onClickForgotPassword,
  isInvalid,
}) {
  const [show, setShow] = useState(false);

  const handleSwitchPassword = () => {
    setShow(!show);
  };

  return (
    <View>
      {forgotPassword ? (
        <Flex direction="row">
          <Text style={styles.titleText}>{title}</Text>
          <Spacer />
          <Text
            style={[styles.forgotPasswordText, fontStyles.h4]}
            onPress={onClickForgotPassword}
          >
            Mot de passe oublié
          </Text>
        </Flex>
      ) : (
        <Text style={[styles.titleText, fontStyles.p2]}>{title}</Text>
      )}
      <FormControl isInvalid={isInvalid}>
        <Input
          type={show ? "text" : "password"}
          variant="underlined"
          placeholder={placeholder}
          placeholderTextColor={colors.grey3}
          value={value}
          onChangeText={onValueChanged}
          borderTopLeftRadius={8}
          borderTopRightRadius={8}
          borderBottomLeftRadius={1}
          borderBottomRightRadius={1}
          borderBottomColor={isInvalid ? colors.red5 : colors.blue3}
          borderBottomWidth={2}
          borderRadius={15}
          fontFamily="Raleway-Regular"
          backgroundColor="white"
          InputRightElement={
            <Pressable onPress={handleSwitchPassword}>
              <View style={styles.iconPassword}>
                {show ? (
                  <HidePasswordIcon height={24} width={24} />
                ) : (
                  <ShowPasswordIcon height={24} width={24} />
                )}
              </View>
            </Pressable>
          }
        />
      </FormControl>
    </View>
  );
}
const styles = StyleSheet.create({
  titleText: {
    color: colors.grey6,
    marginLeft: 4,
    marginBottom: 4,
  },
  iconPassword: {
    marginRight: 16,
    marginTop: 8,
    marginBottom: 8,
  },
  forgotPasswordText: {
    color: colors.purple5,
  },
  errorMessage: {
    color: colors.red5,
    marginLeft: 4,
    fontSize: 12,
    fontFamily: "Helvetica",
  },
});
