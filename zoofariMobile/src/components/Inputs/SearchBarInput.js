import { Input, View } from "native-base";
import React from "react";
import { StyleSheet } from "react-native";
import colors from "../../assets/colors/colors";
import OrangeSearchIcon from "../../assets/icons/IconComponents/OrangeSearchIcon";

export default function SearchBarInput({ value, onValueChanged }) {
  return (
    <View style={styles.searchContainer}>
      <Input
        InputLeftElement={
          <View style={{ paddingLeft: 16 }}>
            <OrangeSearchIcon width={24} height={24} />
          </View>
        }
        placeholder="Rechercher"
        placeholderTextColor={colors.grey2}
        value={value}
        onChangeText={onValueChanged}
        style={styles.searchInput}
        borderRadius={15}
        backgroundColor="white"
      />
    </View>
  );
}
const styles = StyleSheet.create({
  searchContainer: {
    width: "100%",
    elevation: 3,
    borderRadius: 15,
    backgroundColor: "white",
  },
  searchInput: {},
});
