import React from "react";
import { Modal, View, Text } from "native-base";
import Card from "../CollectionScreen/Card";
import { StyleSheet } from "react-native";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function CardMintedModal({
  isOpen,
  handleCloseModal,
  cardInfo,
}) {
  return (
    <Modal isOpen={isOpen} onClose={handleCloseModal}>
      <Modal.Content>
        <Modal.CloseButton />
        <Modal.Header>
          <Text style={fontStyles.h2}>Card created</Text>
        </Modal.Header>
        <Modal.Body>
          <View style={styles.modalContainer}>
            <Card data={cardInfo} />
          </View>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalContainer: {
    marginTop: 20,
    marginBottom: 20,
  },
});
