import React, { useState } from "react";
import { View, Modal, Text } from "native-base";
import CardFoundedList from "../MapScreen/CardFoundedList";
import { useSelector, useDispatch } from "react-redux";
import { Api } from "../../api/index";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function ModalFoundedCards({
  cardList,
  isModalOpen,
  handleCloseModal,
  findModalCard,
}) {
  const [isFinding, setIsFinding] = useState(false);
  let user = useSelector((state) => state.userState);
  let dispatch = useDispatch();

  const handleFind = async (card) => {
    if (user.userId !== undefined) {
      setIsFinding(true);
      let res = await Api.cardService.findCard({ id: card.id }, user.userId);
      if (res) {
        findModalCard(card);
        let newUser = await Api.userService.getUserById(user.userId);
        dispatch({
          type: "UPDATE_GOLDS",
          value: newUser.golds,
        });
      }
      setIsFinding(false);
    }
  };

  return (
    <Modal isOpen={isModalOpen} onClose={handleCloseModal} size="full">
      <Modal.Content>
        <Modal.CloseButton />
        <Modal.Header>
          <Text style={fontStyles.h2}>Cards found</Text>
        </Modal.Header>
        <Modal.Body>
          <CardFoundedList
            cardList={cardList}
            isFinding={isFinding}
            handleFind={handleFind}
          />
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
}
