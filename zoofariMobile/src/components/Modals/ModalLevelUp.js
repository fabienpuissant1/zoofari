import React, { useState, useEffect } from "react";
import { View, Modal, Text, FlatList, Pressable, useToast } from "native-base";
import { useSelector, useDispatch } from "react-redux";
import { StyleSheet } from "react-native";
import { isSacrifiable } from "../../api/final/cardService";
import Card from "../CollectionScreen/Card";
import ConfirmPopup from "../Popup/ConfirmPopup";
import { Api } from "../../api/index";
import { refreshUser } from "../../api/token";
import fontStyles from "../../assets/fontStyles/fontStyles";

export default function ModalLevelUp({
  cardList,
  cardToUp,
  isModalOpen,
  handleCloseModal,
  refreshCards,
}) {
  const [sacrifiableCardList, setSacrifiableCardList] = useState([]);
  let user = useSelector((state) => state.userState);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [sacrifiedCard, setSacrifiedCard] = useState({});
  const [popupBodyText, setPopupBodyText] = useState("");
  const [upgradePrice, setUpgradePrice] = useState(0);
  let toast = useToast();

  useEffect(() => {
    fetchCardsSacrifiable();
  }, [cardList, cardToUp]);

  const fetchCardsSacrifiable = () => {
    let newSacrifiableCards = [];
    for (let i = 0; i < cardList.length; i++) {
      let card = cardList[i];
      if (isSacrifiable(cardToUp, card) && cardToUp.id !== card.id) {
        newSacrifiableCards.push(card);
      }
    }
    setSacrifiableCardList(newSacrifiableCards);
  };

  const handleSelectedSacrified = (card) => {
    setSacrifiedCard(card);
    let newPower = cardToUp.power + 2 * card.power + cardToUp.basePower;
    let price = newPower - cardToUp.power;
    setUpgradePrice(price);
    setPopupBodyText(
      "You will sacrified the selected card to upgrade the other \n The new card power will be : " +
        newPower +
        "\n" +
        "In addition, you will pay : " +
        price +
        " 💰"
    );
    setIsPopupOpen(true);
  };

  const closePopup = () => {
    setIsPopupOpen(false);
  };

  const handleLevelUp = async () => {
    if (user.golds < upgradePrice) {
      toast.show({
        title: "Not enough money",
        status: "error",
        placement: "top",
      });
      setIsPopupOpen(false);
    } else {
      let res = await Api.cardService.levelUpCards(
        user.userId,
        sacrifiedCard.id,
        cardToUp.id
      );
      if (res) {
        toast.show({
          title: "Card upgraded",
          status: "success",
          placement: "top",
        });
        refreshUser();
        setIsPopupOpen(false);
        refreshCards();
        handleCloseModal();
      } else {
        toast.show({
          title: "Server error",
          status: "error",
          placement: "top",
        });
        setIsPopupOpen(false);
      }
    }
  };

  return (
    <>
      <ConfirmPopup
        isOpen={isPopupOpen}
        handleCancel={closePopup}
        handleConfirm={handleLevelUp}
        confirmLabel="Level up"
        headerText="Level up confirmation"
        bodyText={popupBodyText}
      />
      <Modal isOpen={isModalOpen} onClose={handleCloseModal} size="full">
        <Modal.Content>
          <Modal.CloseButton />
          <Modal.Header>
            <Text style={fontStyles.h2}>Level Up</Text>
          </Modal.Header>
          <Modal.Body>
            <View style={styles}>
              <Text style={[fontStyles.p2, { margin: 10 }]}>
                {sacrifiableCardList.length > 0
                  ? "Choose a card to sacrify"
                  : "No card to sacrify"}
              </Text>
              <FlatList
                data={sacrifiableCardList}
                keyboardShouldPersistTaps="always"
                renderItem={({ item }) => (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginTop: 15,
                    }}
                  >
                    <Pressable
                      onPress={() => {
                        handleSelectedSacrified(item);
                      }}
                    >
                      <Card data={item} key={item.id} />
                    </Pressable>
                  </View>
                )}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  labelContainer: {},
});
