import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function CalendarIcon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<Path d="M13.3333 3.33301V8.33301" stroke="#292D32" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M26.6667 3.33301V8.33301" stroke="#292D32" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M5.83325 15.1494H34.1666" stroke="#292D32" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M36.6666 31.6667C36.6666 32.9167 36.3166 34.1 35.6999 35.1C34.5499 37.0333 32.4333 38.3333 29.9999 38.3333C28.3166 38.3333 26.7833 37.7167 25.6166 36.6667C25.0999 36.2333 24.6499 35.7 24.2999 35.1C23.6833 34.1 23.3333 32.9167 23.3333 31.6667C23.3333 27.9833 26.3166 25 29.9999 25C31.9999 25 33.7833 25.8833 34.9999 27.2666C36.0333 28.45 36.6666 29.9833 36.6666 31.6667Z" stroke="#292D32" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M27.3999 31.667L29.0499 33.317L32.5999 30.0337" stroke="#292D32" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M35 14.1663V27.2663C33.7833 25.883 32 24.9997 30 24.9997C26.3167 24.9997 23.3333 27.983 23.3333 31.6663C23.3333 32.9163 23.6833 34.0997 24.3 35.0997C24.65 35.6997 25.1 36.233 25.6167 36.6663H13.3333C7.5 36.6663 5 33.333 5 28.333V14.1663C5 9.16634 7.5 5.83301 13.3333 5.83301H26.6667C32.5 5.83301 35 9.16634 35 14.1663Z" stroke="#292D32" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M19.9924 22.8333H20.0074" stroke="#292D32" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M13.8239 22.8333H13.8389" stroke="#292D32" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M13.8239 27.8333H13.8389" stroke="#292D32" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
</Svg>

)
}