import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function CardsIcon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0, 0, 400,400"><G id="svgg"><Path id="path0" d="" stroke="none" fill="#040404" fillRule="evenodd"></Path><Path id="path1" d="" stroke="none" fill="#080404" fillRule="evenodd"></Path><Path id="path2" d="" stroke="none" fill="#080404" fillRule="evenodd"></Path><Path id="path3" d="" stroke="none" fill="#080404" fillRule="evenodd"></Path><Path id="path4" d="" stroke="none" fill="#080404" fillRule="evenodd"></Path></G></Svg>
)
}