import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function Onboarding1Icon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 375 667" fill="none" xmlns="http://www.w3.org/2000/svg">
<G clipPath="url(#clip0)">
<Rect width="375" height="667" fill="#20C1F4"/>
<Path d="M0 40H375C375 40 396.719 139.081 375 196C322.148 334.507 0 255 0 255V40Z" fill="#FFA66B"/>
<Path d="M0 0H375C375 0 396.719 99.0812 375 156C322.148 294.507 0 215 0 215V0Z" fill="#85DDF9"/>
</G>
<Defs>
<ClipPath id="clip0">
<Rect width="375" height="667" fill="white"/>
</ClipPath>
</Defs>
</Svg>

)
}