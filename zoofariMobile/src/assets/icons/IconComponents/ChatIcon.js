import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function ChatIcon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<Path d="M21.9999 6.25024V11.3502C21.9999 12.6202 21.5799 13.6902 20.8299 14.4302C20.0899 15.1802 19.0199 15.6002 17.7499 15.6002V17.4102C17.7499 18.0902 16.9899 18.5002 16.4299 18.1202L15.4599 17.4802C15.5499 17.1702 15.5899 16.8302 15.5899 16.4702V12.4003C15.5899 10.3603 14.2299 9.00024 12.1899 9.00024H5.39987C5.25987 9.00024 5.12988 9.01026 4.99988 9.02026V6.25024C4.99988 3.70024 6.69988 2.00024 9.24988 2.00024H17.7499C20.2999 2.00024 21.9999 3.70024 21.9999 6.25024Z" stroke="#292D32" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
<Path d="M15.59 12.4V16.47C15.59 16.83 15.55 17.17 15.46 17.48C15.09 18.95 13.87 19.87 12.19 19.87H9.47L6.45 21.88C6 22.19 5.39999 21.86 5.39999 21.32V19.87C4.37999 19.87 3.53 19.53 2.94 18.94C2.34 18.34 2 17.49 2 16.47V12.4C2 10.5 3.18 9.19002 5 9.02002C5.13 9.01002 5.25999 9 5.39999 9H12.19C14.23 9 15.59 10.36 15.59 12.4Z" stroke="#292D32" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
</Svg>

)
}