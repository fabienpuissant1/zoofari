import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function Onboarding2Icon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 375 667" fill="none" xmlns="http://www.w3.org/2000/svg">
<G clipPath="url(#clip0)">
<Rect width="375" height="667" fill="#FF7C3A"/>
<Path d="M0 96H375.338C375.338 96 397.077 206.872 375.338 270.565C322.439 425.555 0 336.586 0 336.586V96Z" fill="#55D0F7"/>
<Path d="M0 0H375.338C375.338 0 397.077 127.962 375.338 201.471C322.439 380.351 0 277.669 0 277.669V0Z" fill="#20C1F4"/>
</G>
<Defs>
<ClipPath id="clip0">
<Rect width="375" height="667" fill="white"/>
</ClipPath>
</Defs>
</Svg>

)
}