import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function BlueArrowRightIcon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<Path fillRule="evenodd" clipRule="evenodd" d="M5.40961 2.18964C5.7025 1.89675 6.17738 1.89675 6.47027 2.18964L10.8169 6.53631C11.6232 7.34253 11.6232 8.65741 10.8169 9.46363L6.47027 13.8103C6.17738 14.1032 5.7025 14.1032 5.40961 13.8103C5.11672 13.5174 5.11672 13.0425 5.40961 12.7496L9.75628 8.40297C9.97672 8.18253 9.97672 7.81741 9.75628 7.59697L5.40961 3.2503C5.11672 2.95741 5.11672 2.48253 5.40961 2.18964Z" fill="#20C1F4"/>
</Svg>

)
}