import React from 'react' 
import Svg, { ClipPath, Defs, G, Path, Rect, Mask, Filter } from 'react-native-svg'; 

export default function LocationIcon({height = 40, width = 40}) {
	return (
<Svg height={height} width={width} viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<Path d="M20 22.3829C22.8719 22.3829 25.2 20.0548 25.2 17.1829C25.2 14.311 22.8719 11.9829 20 11.9829C17.1282 11.9829 14.8 14.311 14.8 17.1829C14.8 20.0548 17.1282 22.3829 20 22.3829Z" stroke="#292D32" strokeWidth="2"/>
<Path d="M6.03326 14.1497C9.31659 0.283655 30.6999 0.266988 33.9666 14.1663C35.8833 22.633 30.6166 29.7997 25.9999 34.233C22.6499 37.4663 17.3499 37.4663 13.9833 34.233C9.38326 29.7997 4.11659 22.6163 6.03326 14.1497Z" stroke="#292D32" strokeWidth="2"/>
</Svg>

)
}