import CleaningIcon from "../icons/IconComponents/CleaningIcon";
import React from "react";
import IroningIcon from "../icons/IconComponents/IroningIcon";
import CarWashIcon from "../icons/IconComponents/CarWashIcon";
import ElectricianIcon from "../icons/IconComponents/ElectricianIcon";
import SmallRepairsIcon from "../icons/IconComponents/SmallRepairsIcon";
import CarRepairIcon from "../icons/IconComponents/CarRepairIcon";
import MassageSPAIcon from "../icons/IconComponents/MassageSPAIcon";
import HaircutIcon from "../icons/IconComponents/HaircutIcon";
import ManicurePedicureIcon from "../icons/IconComponents/ManicurePedicureIcon";
import OldPeopleCareIcon from "../icons/IconComponents/OldPeopleCareIcon";
import BabySittingIcon from "../icons/IconComponents/BabySittingIcon";
import PetSittingIcon from "../icons/IconComponents/PetSittingIcon";
import PlumberIcon from "../icons/IconComponents/PlumberIcon";

export const labels = {
  cleaning: "Ménage",
  ironing: "Repassage",
  carWash: "Nettoyage voiture",

  electrician: "Electricien",
  smallRepairs: "Petites réparations",
  carRepairs: "Réparation voiture",
  plumber: "Plombier",

  massageSpa: "Massage & Spa",
  haircut: "Coiffeur",
  manicurePedicure: "Manicure & Pédicure",

  oldPeopleCare: "Aide Personne agées",
  babysitting: "Babysitting",
  petsitting: "Garde d'animaux",
};

export const services = {
  cleaningServices: {
    title: "Services de ménage",
    data: [
      {
        id: 1,
        icon: <CleaningIcon width={40} height={40} />,
        label: labels.cleaning,
      },
      {
        id: 2,
        icon: <IroningIcon />,
        label: labels.ironing,
      },
      {
        id: 3,
        icon: <CarWashIcon />,
        label: labels.carWash,
      },
      {
        id: 4,
        icon: <CarWashIcon />,
        label: labels.carWash,
      },
      {
        id: 5,
        icon: <CarWashIcon />,
        label: labels.carWash,
      },
    ],
  },
  repairServices: {
    title: "Services de réparation",
    data: [
      {
        id: 1,
        icon: <ElectricianIcon />,
        label: labels.electrician,
      },
      {
        id: 2,
        icon: <SmallRepairsIcon />,
        label: labels.smallRepairs,
      },
      {
        id: 3,
        icon: <CarRepairIcon />,
        label: labels.carRepairs,
      },
      {
        id: 4,
        icon: <PlumberIcon />,
        label: labels.plumber,
      },
    ],
  },
  homeSaloon: {
    title: "Beauté",
    data: [
      {
        id: 1,
        icon: <MassageSPAIcon />,
        label: labels.massageSpa,
      },
      {
        id: 2,
        icon: <HaircutIcon />,
        label: labels.haircut,
      },
      {
        id: 3,
        icon: <ManicurePedicureIcon />,
        label: labels.manicurePedicure,
      },
    ],
  },
  otherServices: {
    title: "Other services",
    data: [
      {
        id: 1,
        icon: <OldPeopleCareIcon />,
        label: labels.oldPeopleCare,
      },
      {
        id: 2,
        icon: <BabySittingIcon />,
        label: labels.babysitting,
      },
      {
        id: 3,
        icon: <PetSittingIcon />,
        label: labels.petsitting,
      },
    ],
  },
};

export const getOnBoardingData = () => {
  let newData = [[], [], [], []];
  let cleaning = services.cleaningServices.data.find((el) => {
    return el.label === labels.cleaning;
  });
  newData[0].push(cleaning);
  let ironing = services.cleaningServices.data.find((el) => {
    return el.label === labels.ironing;
  });
  newData[0].push(ironing);
  let carWash = services.cleaningServices.data.find((el) => {
    return el.label === labels.carWash;
  });
  newData[0].push(carWash);

  let plumber = services.repairServices.data.find((el) => {
    return el.label === labels.plumber;
  });
  newData[1].push(plumber);
  let electrician = services.repairServices.data.find((el) => {
    return el.label === labels.electrician;
  });
  newData[1].push(electrician);
  let smallRepairs = services.repairServices.data.find((el) => {
    return el.label === labels.smallRepairs;
  });
  newData[1].push(smallRepairs);

  let massageSpa = services.homeSaloon.data.find((el) => {
    return el.label === labels.massageSpa;
  });
  newData[2].push(massageSpa);
  let haircut = services.homeSaloon.data.find((el) => {
    return el.label === labels.haircut;
  });
  newData[2].push(haircut);
  let manicurePedicure = services.homeSaloon.data.find((el) => {
    return el.label === labels.manicurePedicure;
  });
  newData[2].push(manicurePedicure);

  return newData;
};
