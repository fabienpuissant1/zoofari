const upcomingServices = [
  {
    id: 1,
    type: "Cleaning",
    date: "24/10/21",
  },
  {
    id: 2,
    type: "Car repair",
    date: "30/10/21",
  },
  {
    id: 3,
    type: "Small repair",
    date: "5/11/21",
  },
];

export default upcomingServices;
