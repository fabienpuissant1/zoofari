const favoriteServices = [
    {
        id: 1,
        icon: "newspaper",
        label: "Manicure & Pedicure",
    },
    {
        id: 2,
        icon: "home",
        label: "Plumber",
    },
    {
        id: 3,
        icon: "plus",
        label: "Add new",
    },
]
export default favoriteServices