import { StyleSheet } from "react-native";

const ralewaySemiBold = "Raleway-SemiBold";
const raleway = "Raleway-Regular";

const fontStyles = StyleSheet.create({
  h1: {
    fontFamily: ralewaySemiBold,
    fontSize: 24,
  },
  h2: {
    fontFamily: ralewaySemiBold,
    fontSize: 20,
  },
  h3: {
    fontFamily: ralewaySemiBold,
    fontSize: 16,
  },
  h4: {
    fontFamily: ralewaySemiBold,
    fontSize: 14,
  },
  h5: {
    fontFamily: ralewaySemiBold,
    fontSize: 10,
  },
  p1: {
    fontFamily: raleway,
    fontSize: 20,
  },
  p2: {
    fontFamily: raleway,
    fontSize: 16,
  },
  p3: {
    fontFamily: raleway,
    fontSize: 14,
  },
  p4: {
    fontFamily: raleway,
    fontSize: 10,
  },
  p5: {
    fontFamily: raleway,
    fontSize: 8,
  },
});

export default fontStyles;
