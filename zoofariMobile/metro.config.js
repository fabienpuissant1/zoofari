// Learn more https://docs.expo.io/guides/customizing-metro
const { getDefaultConfig } = require("expo/metro-config");

const config = getDefaultConfig(__dirname);

// config.resolver.extraNodeModules = {
//   stream: require.resolve("readable-stream"),
//   crypto: require.resolve("react-native-crypto-js"),
// };
// config.transformer = {
//   getTransformOptions: async () => ({
//     transform: {
//       experimentalImportSupport: false,
//       inlineRequires: false,
//     },
//   }),
// };
config.resolver.sourceExts = ["jsx", "js", "ts", "tsx"];

module.exports = config;
