import React, { useState, useEffect } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { registerRootComponent } from "expo";
import CreateAccountPage from "./src/components/Authentification/CreateAccountPage";
import LoginPage from "./src/components/Authentification/LoginPage";
import NavBar from "./src/components/NavBar/NavBar";
import { NativeBaseProvider } from "native-base";
import * as Font from "expo-font";
import { setAuthorizationHeaders } from "./src/api/authorizationHeaders";
import { Provider } from "react-redux";
import store from "./src/redux/store";

const AppNavigator = createStackNavigator(
  {
    Home: NavBar,
    Login: LoginPage,
    CreateAccount: CreateAccountPage,
  },

  {
    initialRouteName: "Home",
    headerMode: "none",
    navigationOptions: {
      headerVisible: false,
    },
  }
);

function App() {
  const AppContainer = createAppContainer(AppNavigator);

  useEffect(() => {
    setAuthorizationHeaders();
  }, []);

  const [isLoaded, setIsLoaded] = useState(false);

  let customFonts = {
    "Raleway-Black": require("./src/assets/fonts/Raleway-Black.ttf"),
    "Raleway-BlackItalic": require("./src/assets/fonts/Raleway-BlackItalic.ttf"),
    "Raleway-Bold": require("./src/assets/fonts/Raleway-Bold.ttf"),
    "Raleway-BoldItalic": require("./src/assets/fonts/Raleway-BoldItalic.ttf"),
    "Raleway-ExtraBold": require("./src/assets/fonts/Raleway-ExtraBold.ttf"),
    "Raleway-ExtraBoldItalic": require("./src/assets/fonts/Raleway-ExtraBoldItalic.ttf"),
    "Raleway-ExtraLight": require("./src/assets/fonts/Raleway-ExtraLight.ttf"),
    "Raleway-ExtraLightItalic": require("./src/assets/fonts/Raleway-ExtraLightItalic.ttf"),
    "Raleway-Italic": require("./src/assets/fonts/Raleway-Italic.ttf"),
    "Raleway-Light": require("./src/assets/fonts/Raleway-Light.ttf"),
    "Raleway-LightItalic": require("./src/assets/fonts/Raleway-LightItalic.ttf"),
    "Raleway-Medium": require("./src/assets/fonts/Raleway-Medium.ttf"),
    "Raleway-MediumItalic": require("./src/assets/fonts/Raleway-MediumItalic.ttf"),
    "Raleway-Regular": require("./src/assets/fonts/Raleway-Regular.ttf"),
    "Raleway-SemiBold": require("./src/assets/fonts/Raleway-SemiBold.ttf"),
    "Raleway-SemiBoldItalic": require("./src/assets/fonts/Raleway-SemiBoldItalic.ttf"),
    "Raleway-Thin": require("./src/assets/fonts/Raleway-Thin.ttf"),
    "Raleway-ThinItalic": require("./src/assets/fonts/Raleway-ThinItalic.ttf"),
  };

  const loadFonts = async () => {
    await Font.loadAsync(customFonts);
    setIsLoaded(true);
  };

  useEffect(() => {
    loadFonts();
  }, []);

  return (
    <Provider store={store}>
      <NativeBaseProvider>
        {isLoaded ? <AppContainer /> : <></>}
      </NativeBaseProvider>
    </Provider>
  );
}
registerRootComponent(App);
export default App;
