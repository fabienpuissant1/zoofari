import "react-native-gesture-handler";
import { registerRootComponent } from "expo";

import App from "./App";
import { LogBox } from "react-native";

LogBox.ignoreLogs([
  "NativeBase: The contrast ratio of 1:1 for darkText on transparent",
  "VirtualizedLists should never be nested",
]); //Hide warnings

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
