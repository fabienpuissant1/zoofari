from flask import Flask
import stomp
from static.activemq.Listener import Listener
from flask import request

app = Flask(__name__)

hosts = [('18.210.123.32', 61613)]
#hosts = [('127.0.0.1', 61613)]


subConn = stomp.Connection(hosts)
subConn.set_listener('', Listener())
subConn.connect('admin', 'admin', wait=True)
subConn.subscribe('/queue/sendPhoto', 123)


@app.route("/postPhoto", methods=['POST'])
def postPhoto():
    conn = stomp.Connection(hosts)
    conn.connect('admin', 'admin', wait=True)
    conn.send('/queue/sendPhoto', request.data.decode('utf-8'))
    conn.disconnect()
    return "true"


    
