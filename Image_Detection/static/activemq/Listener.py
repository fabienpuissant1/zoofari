import json
import static.animal_preprocessing as preprocess
import static.animal_recognition as reco
import static.cartoonGan.run as gan
import requests, base64,json
import os
import stomp


class Listener(stomp.ConnectionListener):

    data=None
    SERVER_URL = "http://54.208.10.17/"

    def on_error(self, message):
        print('received an error "%s"' % message)

    def on_message(self, message):
        self.data = json.loads(message.body)
        print(self.data)
        isRecognized = self.recognize()
        print(isRecognized)
        if(isRecognized):
            self.cartoonize()
            self.sendDataToBack()

    def recognize(self):
        print("recognizing")
        url_path = self.data["imgUrl"]
        #nb_obj = int(preprocess.build_command_line(url_path,count=True,crop=True))
        #if nb_obj != 1:
        #    print("Unable to identify object")
        #    self.triggerError()
        #    return False
        #else:
        predicted_class = reco.image_recognition(url_path)
        print(predicted_class)
        if predicted_class[0][1] > 10 and preprocess.is_animal(predicted_class[0][0]):
            self.data["predictedClass"] = predicted_class[0][0]
            return True
        else:
            print("Unable to predict class")
            self.triggerError()
            return False
        

    def cartoonize(self):
        url_path = self.data["imgUrl"]
        print("cartoon")
        gan.run(url_path)
        img_name = url_path.split("/")[-1]
        filename = os.path.join(os.path.dirname(__file__), "../cartoonGan/cartoonized_images/test.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
        store_url = "http://34.196.125.97/sendPhoto"
        requests.post(store_url,data=json.dumps({"name":img_name,"base64": encoded_string.decode('utf-8')}), headers = {'content-type': 'application/json'})

    def sendDataToBack(self):
        url = self.SERVER_URL + "cards/iaSuccess"
        print(self.data)
        requests.post(url, data=json.dumps(self.data), headers = {'Content-Type': 'application/json'})

    def triggerError(self):
        url = self.SERVER_URL + "cards/iaFailure"
        requests.post(url, data=json.dumps(self.data), headers = {'Content-Type': 'application/json'})

        

