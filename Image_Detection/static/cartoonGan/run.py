import sys
sys.path.append('./static/cartoonGan/White-box-Cartoonization/test_code')
import cartoonize
import os
import requests
import uuid

def run(url_path):
    model_path = r"./static/cartoonGan/White-box-Cartoonization/test_code/saved_models"
    load_folder = r"./static/cartoonGan/source-frames"
    save_folder = r"./static/cartoonGan/cartoonized_images"
    if not os.path.exists(save_folder):
        os.mkdir(save_folder)

    files=os.listdir(save_folder)
    for i in range(0,len(files)):
        os.remove(save_folder+'/'+files[i])

    files=os.listdir(load_folder)
    for i in range(0,len(files)):
        os.remove(load_folder+'/'+files[i])
    
    f = open(load_folder+"/test.jpg", "wb")
    response = requests.get(url_path)
    f.write(response.content)
    f.close()

    
    cartoonize.cartoonize(load_folder, save_folder, model_path)
