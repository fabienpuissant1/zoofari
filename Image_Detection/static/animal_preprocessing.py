import os
from sys import platform as _platform

from cv2 import split

def object_counting(command_line):
    command_line += " --count"
    return command_line

def object_croping(command_line):
    command_line += " --crop"
    return command_line

def build_command_line(img_url, count = False, crop = False):
    if _platform == "linux" or _platform == "darwin":
        python_cmd = "python3"
    elif _platform == "win32" or _platform == "win64":
        python_cmd = "python"
    command_line = python_cmd + " ./static/detect.py --weights ./static/checkpoints/yolov4-416 --size 416 --model yolov4 --images " + img_url
    if count:
        command_line = object_counting(command_line)
    if crop:
        command_line = object_croping(command_line)
    stream = os.popen(command_line)
    if count:
        get_stream_infos = stream.read()
        counted_objects = get_stream_infos.split("\n")[0].split(": ")[1]
    else:
        counted_objects = -1
    return counted_objects
    
def is_animal(detection_names):
    detection_names = detection_names.split(", ")
    a_file = open("./static/animals.txt", "r")
    animal_list = []
    for line in a_file:
        stripped_line = line.strip()
        animal_list.append(stripped_line.lower())
    a_file.close()
    boolean = False
    for detection_name in detection_names:
        separated = detection_name.split(" ")
        for word in separated:
            if word in animal_list:
                boolean = True
    return boolean