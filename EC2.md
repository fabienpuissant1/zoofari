# Deploy on webservice on EC2

## Create an account

Go to https://www.docker.com/ and create an account where your images will be stored

## Build the docker image

Creating a Dockerfile in the root of the project, depending on what kind of server you will deploy

### Node dockerfile

```
FROM node:16-alpine
COPY package.json ./
RUN npm install && mkdir /api && mv ./node_modules ./api
WORKDIR /api
COPY . .
EXPOSE 8080
ENTRYPOINT ["npm", "start"]
```

### Springboot dockerfile

```
FROM  openjdk:11-jdk-alpine
ARG  JAR_FILE=target/*.jar
COPY  ${JAR_FILE}  app.jar
ENTRYPOINT  ["java","-jar","/app.jar"]
```

mvn clean package

## Create image

Go to the root of the app (where Dockerfile is located) then run :

```
docker build -t dockerUserName/exampleservice
```

## Push the image

First log to docker :

```
docker login
```

then push the image :

```
docker push dockerUserName/exampleservice
```

# AWS EC2 config

Create an EC2 instance, then we will configure the instance

### Create an elastic IP

This will fix the ip linked to the instance, so when the instance will restart, its ip will be always the same.
In the left section, scroll down to the elastic Ip section, create one then link it to the instance you have.
Now you can connect to the instance

### Update the instance

```
yum upgrade
```

### Install docker

```
sudo yum install docker
```

```
sudo systemctl enable docker
```

```
sudo systemctl start docker
```

### Login to docker and get your image

```
sudo docker login
```

```
sudo docker pull dockerUserName/exampleservice
```

### Now we will create a service that will be launched on boot

#### Create the file /etc/systemd/system/webserver.service

```
[Unit]
Description=server boot
After=docker.service
[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/home/ec2-user
ExecStartPre=-/usr/bin/docker stop webserver
ExecStartPre=-/usr/bin/docker rm webserver
ExecStartPre=/usr/bin/docker run -d -p 80:8080 --name webserver dockerUserName/exampleservice
ExecStart=/usr/bin/docker exec webserver /bin/sh
[Install]
WantedBy=multi-user.target
```

#### Enable the service on boot

```
sudo systemctl daemon-reload
```

```
sudo systemctl enable webserver.service
```

#### Now you can go the the public ipv4 address (default port 80)

http://ipv4address/

And when you restart the EC2 instance, the webserver will restart too.
