import axios from "axios";

class DistributionManager {
  constructor() {
    this.dailyDistribution = 10000;
    this.frequency = 288; //payment each 5 minutes
    this.roundDistribution = this.dailyDistribution / this.frequency;
    this.CARD_SERVER_URL = "http://54.208.10.17/";
    this.USER_SERVER_URL = "http://34.231.202.160/";
    this.NOTIFICATION_SERVER_URL = "http://34.196.125.97:8081/";

    setInterval(() => {
      this.mainProcess();
    }, 300000);

    if (!DistributionManager.instance) {
      DistributionManager.instance = this;
    }
    return DistributionManager.instance;
  }

  async mainProcess() {
    try {
      let userList = await this.getAllUsers();
      //let totalPower = await this.getTotalPower();
      for (let i = 0; i < userList.length; i++) {
        let user = userList[i];
        let userPower = await this.getTotalPowerOfUser(user.userId);
        let userPart = userPower / 288;
        let goldsToUser = userPart * this.roundDistribution;
        await this.sendGoldToUser(user.userId, goldsToUser);
      }

      axios.post(this.NOTIFICATION_SERVER_URL + "refreshUsers");
    } catch (error) {
      console.log(error);
    }
  }

  async getAllUsers() {
    let res = await axios.get(this.USER_SERVER_URL + "users");
    return res.data;
  }

  //   async getTotalPower() {
  //     let cards = await this.getAllCards();
  //     let totalPower = 0;
  //     cards.forEach((card) => {
  //       totalPower += card.power;
  //     });
  //     return totalPower;
  //   }

  //   async getAllCards() {
  //     let res = await axios.get(this.CARD_SERVER_URL + "cards");
  //     return res.data;
  //   }

  async getTotalPowerOfUser(userId) {
    let cardList = await this.getCardsOfUser(userId);
    let userPower = 0;
    cardList.forEach((card) => {
      userPower += card.power;
    });
    return userPower;
  }

  async getCardsOfUser(userId) {
    let res = await axios.get(this.CARD_SERVER_URL + "cards/user/" + userId);
    return res.data;
  }

  async sendGoldToUser(userId, golds) {
    let res = await axios.post(this.USER_SERVER_URL + "users/addGolds", {
      userId,
      golds,
    });
    return res.data;
  }
}

const instance = new DistributionManager();
Object.freeze(instance);
export default instance;
