import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import DistributionManager from "./DistributionManager.js";

const { PORT = 8082 } = process.env;

const app = express();
app.use(express.json({ extended: false }));
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
