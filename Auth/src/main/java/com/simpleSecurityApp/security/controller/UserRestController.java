package com.simpleSecurityApp.security.controller;

import com.simpleSecurityApp.security.CurrentUser;
import com.simpleSecurityApp.security.message.request.LoginForm;
import com.simpleSecurityApp.security.model.User;
import com.simpleSecurityApp.security.security.services.UserPrinciple;
import com.simpleSecurityApp.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/")
public class UserRestController {

    @Autowired
    UserService userService;

    @GetMapping("UserService/getByEmail/{email}")
    public User getByEmail(@PathVariable String email){
        return userService.getByEmail(email);
    }

    @PutMapping("UserService/updatePassword")
    public boolean updatePassword(@RequestBody String passwords, @CurrentUser UserPrinciple userPrincipal) {
        return userService.updatePassword(passwords, userPrincipal);
    }

    @PutMapping("UserService/updateEmail")
    public boolean updateEmail(@RequestBody LoginForm form, @CurrentUser UserPrinciple userPrinciple){
        return userService.updateEmail(form.getEmail(), userPrinciple);
    }



}
