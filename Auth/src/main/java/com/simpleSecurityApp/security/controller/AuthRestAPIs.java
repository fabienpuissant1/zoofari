package com.simpleSecurityApp.security.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import com.google.gson.JsonObject;
import com.simpleSecurityApp.security.message.request.LoginForm;
import com.simpleSecurityApp.security.message.request.SignUpForm;
import com.simpleSecurityApp.security.message.response.JwtResponse;
import com.simpleSecurityApp.security.model.Role;
import com.simpleSecurityApp.security.model.RoleName;
import com.simpleSecurityApp.security.model.User;
import com.simpleSecurityApp.security.repository.RoleRepository;
import com.simpleSecurityApp.security.repository.UserRepository;
import com.simpleSecurityApp.security.security.jwt.JwtProvider;
import com.simpleSecurityApp.security.service.UserService;
import com.simpleSecurityApp.utils.http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

    private static final String USER_SERVICE_URL = "http://34.231.202.160/";
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

        //Checking if the user email is validated
        User user = userRepository.findByEmail(loginRequest.getEmail()).orElse(null);
        if(user != null){
            if(user.getValidated()){
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                loginRequest.getEmail(),
                                loginRequest.getPassword()
                        )
                );

                SecurityContextHolder.getContext().setAuthentication(authentication);

                String jwt = jwtProvider.generateJwtToken(authentication);
                return ResponseEntity.ok(new JwtResponse(jwt));
            }
        }
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body("Compte inexistant ou pas encore validé");

    }

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<String>("Fail -> Email is already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getEmail(),
                        encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
        	switch(role) {
	    		case "admin":
	    			Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
	    			roles.add(adminRole);
	    			
	    			break;
	    		case "pm":
	            	Role pmRole = roleRepository.findByName(RoleName.ROLE_PM)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
	            	roles.add(pmRole);
	            	
	    			break;
	    		default:
	        		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
	                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
	        		roles.add(userRole);        			
        	}
        });
        userRepository.save(user);
        JsonObject json = new JsonObject();
        json.addProperty("userId", user.getId());
        json.addProperty("name", signUpRequest.getName());
        json.addProperty("surname", signUpRequest.getSurname());
        if(http.executePost(USER_SERVICE_URL + "users", json)){
            user.setRoles(roles);
            return ResponseEntity.ok().body("User registered successfully!");
        } else {
            userRepository.delete(user);
            return ResponseEntity.badRequest().body("User registered successfully!");
        }

    }
}