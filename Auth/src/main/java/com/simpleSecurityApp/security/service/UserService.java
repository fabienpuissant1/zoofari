package com.simpleSecurityApp.security.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.simpleSecurityApp.security.model.User;
import com.simpleSecurityApp.security.repository.UserRepository;
import com.simpleSecurityApp.security.security.services.UserPrinciple;
import com.simpleSecurityApp.utils.StringFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElse(null);
    }


    /*
    jsonnewPassword= {
        "oldPassword": ,
        "password":  ,
    }
     */
    public boolean updatePassword(String jsonnewPassword, UserPrinciple userPrincipal){
        User user = userRepository.findById(userPrincipal.getId()).orElse(null);

        if(user != null) {
            if (this.checkPassword(jsonnewPassword, userPrincipal)) {
                JsonObject jsonObject = new JsonParser().parse(jsonnewPassword).getAsJsonObject();
                String password = jsonObject.get("password").toString();
                password = password.substring(1, password.length() - 1);
                String encodedPass = passwordEncoder.encode(password);
                user.setPassword(encodedPass);
                userRepository.save(user);
                return true;
            }
        }
        return false;
    }

    public Boolean checkPassword(String jsonpassword, UserPrinciple userPrincipal){
        System.out.println(jsonpassword);
        JsonObject jsonObject = new JsonParser().parse(jsonpassword).getAsJsonObject();
        String password = jsonObject.get("oldPassword").toString();
        password = password.substring(1, password.length() - 1);
        return passwordEncoder.matches(password, userPrincipal.getPassword());
    }

    public boolean updateEmail(String email, UserPrinciple userPrinciple) {
        User user = userRepository.findById(userPrinciple.getId()).orElse(null);
        if(user != null){
            user.setEmail(email);
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
