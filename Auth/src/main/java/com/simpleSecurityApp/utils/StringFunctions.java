package com.simpleSecurityApp.utils;

public class StringFunctions {

    public static String removeSpecialChar(String string){
        String allowedCharacters = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ'-";
        return string.replaceAll("[^a-zA-Z0-9" + allowedCharacters + "]", " ");
    }

    public static String removeSpecialCharPhone(String string){
        String allowedCharacters = "+";
        return string.replaceAll("[^0-9" + allowedCharacters + "]", " ");
    }

    public static String removeSpecialCharZipCode(String string) {
        return string.replaceAll("[^0-9]", " ");
    }
}
