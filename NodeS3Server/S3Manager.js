import {
  S3Client,
  UploadPartCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3";

class S3Manager {
  s3Bucket;
  AWS_BUCKET_NAME = "accesspoint-ari7r37zjeb5yucbjzkedqr538py1euw3a-s3alias";
  AWS_ID = "AKIAQ2D7LKQQPSGHLAGN";
  AWS_SECRET = "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";

  S3_ENTRY_POINT = "https://zoofariphotos.s3.eu-west-3.amazonaws.com/";

  constructor() {
    this.client = new S3Client({
      region: "eu-west-3",
      credentials: {
        accessKeyId: this.AWS_ID,
        secretAccessKey: this.AWS_SECRET,
      },
    });
  }

  async upload(photo) {
    return new Promise(async (resolve, reject) => {
      try {
        let base64Image = photo.base64;
        const imgBuffer = Buffer.from(base64Image, "base64");

        let uploadParams = {
          Bucket: this.AWS_BUCKET_NAME,
          Key: photo.name,
          Body: imgBuffer,
        };
        const command = new UploadPartCommand(uploadParams);
        await this.client.send(command);
        resolve();
      } catch (e) {
        reject();
      }
    });
  }

  async remove(photo) {
    let parts = photo.imgUrl.split("/");
    let imgName = parts[parts.length - 1];
    let uploadParams = {
      Bucket: this.AWS_BUCKET_NAME,
      Key: imgName,
    };
    const command = new DeleteObjectCommand(uploadParams);
    await this.client.send(command);
  }
}

const instance = new S3Manager();
Object.freeze(instance);

export default instance;
