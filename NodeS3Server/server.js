import express from "express";
import cors from "cors";
import S3Manager from "./S3Manager.js";
import bodyParser from "body-parser";

const { PORT = 8080 } = process.env;

const app = express();
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json({ extended: false }));
app.use(cors({ origin: "*" }));

app.post("/sendPhoto", (req, res) => {
  S3Manager.upload(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      res.sendStatus(500);
    });
});

app.post("/removePhoto", (req, res) => {
  S3Manager.remove(req.body)
    .then(() => {
      res.sendStatus(200);
    })
    .catch((err) => {
      res.sendStatus(500);
    });
});

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
